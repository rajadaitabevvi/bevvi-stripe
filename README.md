Stripe Backend Module for Bevvi

Testing Card Numbers :

Card Numbers are divided in to following categories:

1. Success full payment
4000 0000 0000 3220 - Requires 3D Secure 2 Authentication
4000 0000 0000 3063 - Requires 3D Secure Authentication
4000 0027 6000 3184 - Requires 3D Secure Authentication
3622 7206 2716 67   - 14 digit Diner's Card without 3D Secure
3782 8224 6310 005  - American Express with 4 digit cvc
5105 1051 0510 5100 - Master card prepaid
4000 0035 6000 0008 - India visa card
4000 0048 4000 8001 - Mexico visa card
4000 0012 4000 0000 - Canada visa card
4000 0007 6000 0002 - Brazil visa card
4000 0015 6000 0002 - Chind visa card
4000 0000 0000 0010 - Charge succeed with address verification fails

2. Charge Fails
4000 0082 6000 3178 - 3D Secure Authentication with card error as insufficient funds
4000 0000 0000 0341 - card get added to customer account but charge will fail

3. Charge Succeed but capture fails
4000 0000 0000 0259 - Charge succeeds and dispute as fraudulent

4. Creating New card
4000 0084 0000 1629 - 3D Secure Authentication with card decline error
4000 0000 0000 0069 - Card is expired
4000 0000 0000 9979 - Card is declined because of stolen card.
4000 0000 0000 0127 - Card is decline with incorrect cvc code.

5. Other Type of Cards
4242 4242 4242 4242 - support 3D Secure but will only ask 3D Secure if radar rules request it.
