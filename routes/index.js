/**
 * Bevvi Strip Module.
 * Module used to connect with Stripe account of platform, connected and customer.
 * StripeAPI is used for integration with Stripe
 * @author: SHRADESH PAIYYAR <shradesh@lotusis.com>
 */
var Taxjar = require('taxjar');
var BEVVI_STRIPE_ACC = "acct_1BgFGcIflouKn1E7";
// DEVELOPMENT
if (process.env.NODE_ENV === 'development') {
    var API_ENDPOINT = "http://dev.getbevvi.com:3000/api/";
    var B2B_API_ENDPOINT = "http://dev.getbevvi.com:4000/api/";
    // var B2B_API_ENDPOINT = "http://localhost:4000/api/";
}

if (process.env.NODE_ENV === 'local') {
    var B2B_API_ENDPOINT = "http://localhost:4000/api/";
}

//PRODUCTION
if (process.env.NODE_ENV === 'production') {
    var API_ENDPOINT = "https://getbevvi.com:3000/api/";
    var B2B_API_ENDPOINT = "https://api.getbevvi.com/api/";
}

var GOOGLE_API_KEY = "AIzaSyDzrB6apvcw7xd3zCNwGNHWtKitGCGjg9A";
if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'local') {
    BEVVI_STRIPE_ACC = "acct_1BgFGcIflouKn1E7";
    var STRIPE_SK = "sk_test_okUZQYhWtpN830a9SKN0QkcU";
}

if (process.env.NODE_ENV === 'production') {
    BEVVI_STRIPE_ACC = "acct_1BgFGcIflouKn1E7"
    var STRIPE_SK = "sk_live_4LNNhL6dA0KqHNG17PjV4b6z";
}
var TAXJAR_API_KEY = "0620fddcaa5cc02fb8cbcbd71be8a0e5";
var BUCKET_NAME = "bevvi";
var ACCESS_KEY = "K5KqoNOEaz94HIeRZoJxefub05V3IAoVD+qKgxIZ";
var KEY_ID = "AKIAIY23VBHNXDTGOSHA";
var REGION = "us-east-2";
var LIGHTSPEED_CLIENT_KEY = "c88c0ae4fbc7d7e42a0dcfbd699b31614bef15e124f0a49ff67e295273c20e98";
var LIGHTSPEED_CLIENT_SECRET = "5df08d273bed3fb2d997056995f9dfcc61aa58bec9ea6935232d2f1a561f671a";

var express = require('express');
var aws = require('aws-sdk');
var bodyParser = require('body-parser');
var multer = require('multer');
var multerS3 = require('multer-s3');
var express = require('express');
var request = require('request');
var fs = require("fs");
var path = require("path");
var async = require("async");
var successlog = require('../util/logger').successlog;
const { url } = require('inspector');
var stripe = require("stripe")(STRIPE_SK,{
    apiVersion: '2020-03-02'
});
var taxjar = new Taxjar({apiKey: TAXJAR_API_KEY});

var router = express.Router();
var maxmindThreshold = 1;


aws.config.update({
    secretAccessKey: ACCESS_KEY,
    accessKeyId: KEY_ID,
    region: REGION
});

var app = express();
var s3 = new aws.S3();

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: BUCKET_NAME,
        key: function (req, file, cb) {
            console.log(file);
            cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + Date.now() + path.extname(file.originalname)); //use Date.now() for unique file keys
        }
    })
});


router.get('/list-accounts', function (req, res, next) {
    listAllAccount()
    .then(function(list) {
        console.log(JSON.stringify(list));
        res.status(200).send(JSON.stringify(list));
        res.end();
    })
})

/**
 * Customer Router Functions: CRU, List
 */
router.post('/create-b2b-customer', function (req, res, next) {
    successlog.info(`create-b2b-customer :: ${JSON.stringify(req.body)}`);
    console.log("create-b2b-customer req.body", req.body);

    createCustomer(req.body.email)
    .then(function(customer) {
        console.log("create-b2b-customer :: success :: ", customer);
        successlog.info(`create-b2b-customer :: success :: ${JSON.stringify(customer)}`);
        var options = {
            method: 'POST',
            url: B2B_API_ENDPOINT + 'paymentIds',
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            form: {
                accountId: req.body.accountId,
                token: customer.id,
                type: 'stripe'
            }
        };

        request(options, function (error, response, body) {
            if (error) {
                successlog.info(`create-customer :: error :: ${JSON.stringify(error)}`);
                successlog.info(`create-customer :: error :: ${JSON.stringify(body)}`);
                // return true;
                res.end();
            } else {
                successlog.info(`create-customer :: success :: ${JSON.stringify(body)}`);
                // return true;
                res.end();
            }
        });
    }).catch(function(err) {
        console.log("create-b2b-customer :: error :: ", JSON.stringify(err));
        successlog.info(`create-b2b-customer :: error :: ${JSON.stringify(err)}`);
        res.send(JSON.stringify(err));
        res.end();
    });
});

router.post('/update_customer', function (req, res, next) {
    console.log("update_customer req.body", req.body);
    let idType;
    if (req.body.sourceId.substr(0,3) === 'src') {
        idType = 'source';
    } else {
        idType = 'payment_methods';
    }

    let updateData = {
        customerId: req.body.customerId,
        sourceId: req.body.sourceId,
        type: idType
    }

    updateCustomerSourceInfo(updateData)
    .then((customer) => {
        console.log("update_customer :: ", customer);
        res.status(200).send(JSON.stringify(customer)).end();
    })
    .catch((err) => {
        console.log("update_customer :: ", JSON.stringify(err));
        res.status(err.statusCode).send(JSON.stringify(err)).end();
    });
});

router.post('/retrieve_customer', function (req, res, next) {
    console.log("retrieve_customer req.body", req.body);

    retrieveCustomer(req.body.customerId)
    // listAllPaymentMethod(req.body.customerId)
    .then(function(customers) {
        console.log("retrieve_customer -->1:: ", customers);
        console.log("retrieve_customer :: ", JSON.stringify(customers));
        res.status(200).send(JSON.stringify(customers));
        // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        res.end();
    })
    .catch(function(err) {
        console.log("retrieve_customer :: ", JSON.stringify(err));
        res.status(err.statusCode).send(JSON.stringify(err));
        res.end();
    });
});

router.post('/list-customer', function (req, res, next) {
    console.log("list-customer req.body", req.body);
    console.log("list-customer req.body", JSON.stringify(req.body));
    var filter = {
        limit: 1
    }
    if (req.body.email) {
        filter.email = req.body.email;
    }
    stripe.customers.list(filter, function (err, customers) {
        if (err) {
            console.log("list-customer :: ", JSON.stringify(err));
            res.send(JSON.stringify(err));
            res.end();
        } else {
            console.log("list-customer :: ", customers);
            res.send(JSON.stringify(customers));
            res.end();
        }
    });
});

/**
 * Card Router Function: CRUD, List (Source & Payment Methods)
 */
router.post('/create_card', function (req, res, next) {
    console.log("create_card req.body name", req.body.name);
    console.log("create_card req.body zip", req.body.zipcode);
    let sourceInfo = {
        customerId: req.body.customerId,
        source: {
            object: 'source',
            type: "card",
            card: {
                exp_year: req.body.exp_year,
                number: req.body.number,
                cvc: req.body.cvc,
                exp_month: req.body.exp_month
            },
            owner: {
                name: req.body.name,
                email: req.body.email ? req.body.email: '',
                phone: req.body.phone ? req.body.phone: '',
                address: {
                    line1: req.body.address ? req.body.address : '',
                    line2: req.body.apt ? req.body.apt : '',
                    city: req.body.city ? req.body.city : '',
                    state: req.body.state ? req.body.state : '',
                    country: req.body.country ? req.body.country : 'US',
                    postal_code: req.body.zipcode
                }
            }
        }
    }

    // createCard(sourceInfo)
    createPaymentMethod(sourceInfo)
    .then((card) => {
        // console.log("create_card :: success :: ", card);
        console.log("payment_method :: success :: ", card);
        sourceInfo.paymenet_method_id = card.id;
        
        attachPaymentMethod(sourceInfo)
        .then((attach) => {
            console.log(attach);
            let updateData = {
                customerId: req.body.customerId,
                sourceId: card.id,
                type: 'payment_method'
            }
            updateCustomerSourceInfo(updateData)
            .then((customer) => {
                // setupPaymentIntents(sourceInfo)
                // .then((setup) => {
                //     console.log('Setup Paymnet Intent response');
                //     console.log(setup)
                //     if (setup.status === "succeeded") {
                //         console.log("update_customer :: ", customer);
                //         res.status(200).send({"status":"success","card":JSON.stringify(card)}).end();
                //     } else if (setup.status === 'requires_action') {
                //         res.status(200).send(JSON.stringify(setup)).end();
                //     } else {

                //     }
                // })
                // .catch((error) => {
                //     console.log('Error in setup paymnet intent')
                //     console.log(error);
                // });
                console.log("update_customer :: ", customer);
                res.status(200).send(JSON.stringify(card)).end();
            })
            .catch((err) => {
                console.log("update_customer :: ", JSON.stringify(err));
                res.status(err.statusCode).send(JSON.stringify(err)).end();
            });
        })
        .catch((error) => {
            console.log("create_card :: error ::", error);
            res.status(error.statusCode).send(JSON.stringify(error));
            res.end();
        })
    })
    .catch((error) => {
        console.log("create_card :: error ::", error);
        res.status(error.statusCode).send(JSON.stringify(error));
        res.end();
    });
});

router.post('/create_card_corp', function (req, res, next) {
    console.log("create_card req.body name", req.body.name);
    console.log("create_card req.body zip", req.body.zipcode);
    let sourceInfo = {
        customerId: req.body.customerId,
        source: {
            object: 'source',
            type: "card",
            card: {
                exp_year: req.body.exp_year,
                number: req.body.number,
                cvc: req.body.cvc,
                exp_month: req.body.exp_month
            },
            owner: {
                name: req.body.name,
                email: req.body.email ? req.body.email: '',
                phone: req.body.phone ? req.body.phone: '',
                address: {
                    line1: req.body.address ? req.body.address : '',
                    line2: req.body.apt ? req.body.apt : '',
                    city: req.body.city ? req.body.city : '',
                    state: req.body.state ? req.body.state : '',
                    country: req.body.country ? req.body.country : 'US',
                    postal_code: req.body.zipcode ? req.body.zipcode : ''
                }
            }
        }
    }

    // createCard(sourceInfo)
    createPaymentMethodCorporate(sourceInfo)
    .then((card) => {
        // console.log("create_card :: success :: ", card);
        console.log("payment_method :: success :: ", card);
        sourceInfo.paymenet_method_id = card.id;
        
        attachPaymentMethod(sourceInfo)
        .then((attach) => {
            console.log(attach);
            let updateData = {
                customerId: req.body.customerId,
                sourceId: card.id,
                type: 'payment_method'
            }
            updateCustomerSourceInfo(updateData)
            .then((customer) => {
                // setupPaymentIntents(sourceInfo)
                // .then((setup) => {
                //     console.log('Setup Paymnet Intent response');
                //     console.log(setup)
                //     if (setup.status === "succeeded") {
                //         console.log("update_customer :: ", customer);
                //         res.status(200).send({"status":"success","card":JSON.stringify(card)}).end();
                //     } else if (setup.status === 'requires_action') {
                //         res.status(200).send(JSON.stringify(setup)).end();
                //     } else {

                //     }
                // })
                // .catch((error) => {
                //     console.log('Error in setup paymnet intent')
                //     console.log(error);
                // });
                console.log("update_customer :: ", customer);
                res.status(200).send(JSON.stringify(card)).end();
            })
            .catch((err) => {
                console.log("update_customer :: ", JSON.stringify(err));
                res.status(err.statusCode).send(JSON.stringify(err)).end();
            });
        })
        .catch((error) => {
            console.log("create_card :: error ::", error);
            res.status(error.statusCode).send(JSON.stringify(error));
            res.end();
        })
    })
    .catch((error) => {
        console.log("create_card :: error ::", error);
        res.status(error.statusCode).send(JSON.stringify(error));
        res.end();
    });
});

router.post('/retrieve_card', function (req, res, next) {
    console.log("retrieve_card req.body", req.body);
    let sourceInfo = {
        customerId: req.body.customerId,
        sourceId: req.body.sourceId
    };

    retrieveCard(sourceInfo)
    .then((card) => {
        console.log("retrieve_card :: ", card);
        res.status(200).send(JSON.stringify(card)).end();
    })
    .catch((error) => {
        console.log("retrieve_card :: error :: ", error);
        res.status(error.statusCode).send(JSON.stringify(error)).end();
    });
});

router.post('/update_card', function(req, res, next) {
    console.log("update_card req.body", req.body);
    if (req.body.customerId && req.body.sourceId) {
        let sourceInfo = {
            customerId: req.body.customerId,
            sourceId: req.body.sourceId,
            source : {
                exp_year: req.body.exp_year ? req.body.exp_year : '',
                exp_month: req.body.exp_month ? req.body.exp_month : '',
                postal_code: req.body.zipcode ? req.body.zipcode : '',
                name: req.body.name ? req.body.name : '',
            }
        };

        updateCard(sourceInfo)
        .then((card) => {
            console.log("update_card :: success ::", card);
            res.status(200).send(JSON.stringify(card)).end();
        })
        .catch((error) => {
            console.log("update_card :: error :: ", error);
            res.status(error.statusCode).send(JSON.stringify(error)).end();
        })
    } else {
        res.status(400).send('Validation Error').end();
    }
})

router.post('/delete_card', function (req, res, next) {
    console.log("delete_card req.body", req.body);
    let sourceInfo = {
        customerId: req.body.customerId,
        sourceId: req.body.sourceId
    };

    // deleteCard(sourceInfo)
    detachPaymentMethod(req.body.sourceId)
    .then((source) => {
        console.log("delete_card :: success :: ",  source);
        res.status(200).send(JSON.stringify(source)).end();
    })
    .catch((error) => {
        console.log("delete_card :: error :: ", error);
        res.status(error.statusCode).send(JSON.stringify(error)).end();
    });
});

router.post('/list_source_cards', function(req, res, next) {
    console.log("list_card  req.body ",req.body);
    
    listAllSourceCard(req.body.customerId, req.body.limit)
    .then((card) => {
        console.log("list_card :: success :: ", JSON.stringify(card));
        res.status(200).send(JSON.stringify(card)).end();
    })
    .catch((error) => {
        console.log("list_card :: error :: ", JSON.stringify(error));
        res.status(error.statusCode).send(JSON.stringify(error)).end();
    })
});

router.post('/list_payment_methods', function (req, res, next) {
    console.log("list_payment_methods req.body", req.body);

    listAllPaymentMethod(req.body.customerId)
    .then(function(paymentMethods) {
        console.log("payment_methods :: success :: ", JSON.stringify(paymentMethods));
        res.status(200).send(JSON.stringify(paymentMethods));
        res.end();
    })
    .catch(function(err) {
        console.log("payment_methods :: error :: ", JSON.stringify(err));
        res.status(err.statusCode).send(JSON.stringify(err));
        res.end();
    });
});


/**
 * Stripe PaymentIntents functions: Create Charge or Single Order & Create Charge Multiple for Multiple Order
 */
router.post('/create_charge_b2b', function (req, res, next) {
    var shoppingCartBody = {};
    var chargeId = "";
    var transactionId = "";
    var orderId = "";
    var tmpTotalPrice = 0;
    var deliveryLocation = {};
    let ipAddress;
    let userAgent;
    let browserAcceptLanguage;

    // console.log(req);
    if (req._remoteAddress.substr(0,7) === "::ffff:") {
        ipAddress = req._remoteAddress.substr(7);
    }
    // Comment these line when using on dev server and prod server
    if (process.env.NODE_ENV === 'development') {
        // ipAddress = '103.255.182.250';
    }
    

    Object.entries(req.headers).forEach((key,res) => {
        if (key[0] === "user-agent") {
            userAgent = key[1];
        } else if (key[0] === "accept-language") {
            browserAcceptLanguage = key[1];
        }
    })
    console.log("create_charge_b2b req.body", req.body);
    successlog.info(`create_charge_b2b :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'GET',
        url: B2B_API_ENDPOINT + 'accounts?access_token=' + req.body.accessToken + '&filter={"where":{"id":"' + req.body.accountId + '"}}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    };

    request(options, function (error, response, body) {
        if (error) {

        } else {
            if (JSON.parse(body).error) {
                res.send({ "message": JSON.parse(body).error.message, "status": "error" }).end();
            } else {
                var options = {
                    method: 'GET',
                    url: B2B_API_ENDPOINT + 'deliverylocations?access_token=' + req.body.accessToken + '&filter={"where":{"accountId":"' + req.body.accountId + '"}}',
                    headers: { 'content-type': 'application/x-www-form-urlencoded' }
                };

                request(options, function (error, response, body) {
                    if (error) {

                    } else {
                        if (JSON.parse(body).error) {
                            res.send({ "message": JSON.parse(body).error.message, "status": "error" }).end();
                        } else {
                            deliveryLocation = JSON.parse(body)[0];
                            //FETCH SHOPPING CART
                            var options = {
                                method: 'GET',
                                url: B2B_API_ENDPOINT + 'shoppingcarts?filter={"where":{"accountId":"' + req.body.accountId + '","state":0},"include":["offer"]}',
                                headers: { 'content-type': 'application/x-www-form-urlencoded' }
                            };

                            request(options, function (error, response, body) {
                                if (error) {
                                    successlog.info(`create_charge_b2b :: USERS SHOPPING CART :: error :: ${JSON.stringify(error)}`);
                                    successlog.info(`create_charge_b2b :: USERS SHOPPING CART :: error :: ${JSON.stringify(body)}`);
                                } else {
                                    successlog.info(`create_charge_b2b :: USERS SHOPPING CART :: ${JSON.stringify(body)}`);
                                    console.log(JSON.stringify(body));
                                    shoppingCartBody = JSON.parse(body);

                                    shoppingCartBody.forEach(function (item) {
                                        tmpTotalPrice += item.offer.salePrice * item.quantity;
                                    });

                                    let finalTotalPrice = parseInt(((((tmpTotalPrice - req.body.discountApplied.discountAmount) + parseFloat(req.body.taxAmount)) + parseFloat(req.body.deliveryFee) + parseFloat(req.body.tipAmount)) * 100).toFixed(2));
                                    successlog.info(`++++++++++++++++++++++++++++++++ Amount from web ${req.body.amount}`);
                                    successlog.info(`++++++++++++++++++++++++++++++++ finalTotalPrice ${finalTotalPrice}`);

                                    if (tmpTotalPrice < 1) {
                                        res.send({ "discountUsed": "Total Amount is less than $1.00. Order cannot be placed", "status": "error" }).end();
                                    } else {
                                        let charge = null;
                                        let err = null;
                                        let paymentInfo = {
                                            amount: req.body.amount,
                                            currency: "usd",
                                            customer: req.body.customerId,
                                            source: req.body.source,
                                            destination_account: req.body.destination_account,
                                            accountId: req.body.accountId,
                                            email: req.body.email,
                                            taxAmount: req.body.taxAmount,
                                            taxPercent: req.body.taxPercent,
                                            tipAmount: req.body.tipAmount,
                                            discountApplied: req.body.discountApplied,
                                            promoCode: req.body.promoCode,
                                            deliveryFee: req.body.deliveryFee,
                                            shippingFee: req.body.shippingFee,
                                            isShipping: req.body.isShipping,
                                            orderForOther: req.body.orderForOther,
                                            orderPhoneNumber: req.body.orderPhoneNumber,
                                            establishmentId: req.body.establishmentId,
                                            deliveryDateTime: req.body.deliveryDateTime,
                                            deliveryInstructions: req.body.deliveryInstructions ? req.body.deliveryInstructions : '',
                                            suitNumber: req.body.suitNumber,
                                            ipAddress: ipAddress,
                                            browserAcceptLanguage: browserAcceptLanguage,
                                            userAgent: userAgent,
                                            accessToken: req.body.accessToken,
                                            shoppingCart: shoppingCartBody,
                                            deliveryLocation: deliveryLocation,
                                            multiple: false,
                                            count: 0,
                                        }

                                        if (req.body.paymentConfirmed) {
                                            if (req.body.paymentIntent) {
                                                paymentInfo.status = 0;
                                                paymentInfo.paymentIntent = req.body.paymentIntent;
                                                if (process.env.NODE_ENV === 'production') {
                                                    console.log('Send Data to nofraud');

                                                    var options = {
                                                        url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                                        method: 'POST',
                                                        headers: { 'content-type': 'application/json' },
                                                        json: true,
                                                        body: {
                                                            orderInfo: paymentInfo
                                                        }
                                                    };
                        
                                                    request(options, function (error, response, body) {
                                                        if (error) {
                                                            console.log(error);
                                                            res.status(400).send(error);
                                                        } else if (response.statusCode === 200) {
                                                            // let riskScore = body.maxmindRiskScore;
                                                            // console.log('Maxmind Score is ', riskScore)
                                                            let decision = body.decision;
                                                            // if (riskScore != -1 && riskScore > 0 && riskScore <= maxmindThreshold) {
                                                            if (decision === 'pass') {
                                                                paymentInfo.riskScore = decision;
                                                                paymentInfo.decision = body.decision;
                                                                paymentInfo.noFraudId = body.id;
                                                                // paymentInfo.maxmindRiskScore = body.maxmindRiskScore;
                                                                transactionOrder(res, req.body.paymentIntent, '', paymentInfo, false, chargeCallBack);
                                                            } else {
                                                                cancelingOrder(res, paymentInfo, decision);
                                                            }
                                                        } else {
                                                            res.status(400).send({'status': 'error', 'message': 'Error'});
                                                        }
                                                    });
                                                } else {
                                                    console.log('Send Data to nofraud');
                                                    paymentInfo.decision = "pass";
                                                    paymentInfo.noFraudId = 123;
                                                    paymentInfo.maxmindRiskScore = 0.1;
                                                    transactionOrder(res, req.body.paymentIntent, '', paymentInfo, false, chargeCallBack);
                                                    // var options = {
                                                    //     url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                                    //     method: 'POST',
                                                    //     headers: { 'content-type': 'application/json' },
                                                    //     json: true,
                                                    //     body: {
                                                    //         orderInfo: paymentInfo
                                                    //     }
                                                    // };
                        
                                                    // request(options, function (error, response, body) {
                                                    //     if (error) {
                                                    //         console.log(error);
                                                    //         res.status(400).send(error);
                                                    //     } else if (response.statusCode === 200) {
                                                    //         console.log(body);
                                                    //         if (body.decision === 'pass' || body.decision === 'review') {
                                                    //             paymentInfo.decision = body.decision;
                                                    //             paymentInfo.noFraudId = body.id;
                                                    //             paymentInfo.maxmindRiskScore = body.maxmindRiskScore;
                                                    //             transactionOrder(res, req.body.paymentIntent, '', paymentInfo, chargeCallBack);
                                                    //         } else {
                                                    //             cancelingOrder(res, paymentInfo, riskScore);
                                                    //         }
                                                    //     } else {
                                                    //         res.status(400).send({'status': 'error', 'message': 'Error'});
                                                    //     }
                                                    // });
                                                    // setupPaymentIntents(paymentInfo)
                                                    // .then((setupIntent) => {
                                                    //     console.log('Setup Payment Intent');
                                                    //     console.log(setupIntent);
                                                    // })
                                                    // .catch((error) => {
                                                    //     console.log('Error Setup payment Intent');
                                                    //     console.log(error);
                                                    // })
                                                    
                                                }
                                                
                                            } else {
                                                transactionOrder(res, '', req.body.paymentIntentError, '', paymentInfo, false, chargeCallBack);
                                            }
                                            

                                        } else {
                                            try {
                                                cloningPaymentMethod(paymentInfo)
                                                .then((source) => {
                                                    console.log('Cloning Source Result', source);
                                                    paymentInfo.payment_method = source.id;
                                                            createPaymentIntents(paymentInfo)
                                                            .then(function(charge) {
                                                                console.log(charge);
                                                                console.log('CREATE PAYMENT INTENTS')
                                                                if (charge.status === 'requires_confirmation') {
                                                                    res.status(200).send(charge);
                                                                } else if (charge.status === 'requires_capture') {
                                                                    transactionOrder(res, confirm, '', paymentInfo, false, chargeCallBack);
                                                                } else {
                                                                    res.status(400).send({"status":"error", "message":'Payment cannot process'});
                                                                }
                                                            }).catch(function(err) {
                                                                console.log('Error Creating Payment Intent');
                                                                console.log(err);
                                                                transactionOrder(res, '', err, paymentInfo, false, chargeCallBack);
                                                            });
                                                })
                                                .catch((err) => {
                                                    // console.log('Cloning Source Failed');
                                                    console.log('Cloning Payment Method failed')
                                                    console.log(err);
                                                    res.status(err.statusCode).send(JSON.stringify(err)).end();
                                                });
                                                
                                            } catch (err) {
                                                console.log('Error');
                                                res.status(200).send({"mesage: ":'Error fetching details'}).end();
                                            }
                                        }
                                        
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});

router.post('/create_charge_multiple', function (req, res, next) {
    console.log('Create Multiple Charge');

    var shoppingCartBody1 = [];
    var shoppingCartBody2 = [];
    var chargeId = "";
    var transactionId = "";
    var orderId = "";
    var tmpTotalPrice = 0;
    let ipAddress;
    let userAgent;
    let browserAcceptLanguage;

    // console.log(req);
    if (req._remoteAddress.substr(0,7) === "::ffff:") {
        ipAddress = req._remoteAddress.substr(7);
    }
    // Comment these line when using on dev server and prod server
    if (process.env.NODE_ENV === 'development') {
        // ipAddress = '103.255.182.250';
    }

    Object.entries(req.headers).forEach((key,res) => {
        if (key[0] === "user-agent") {
            userAgent = key[1];
        } else if (key[0] === "accept-language") {
            browserAcceptLanguage = key[1];
        }
    })
    console.log('Order Additional Data assigned',ipAddress,userAgent,browserAcceptLanguage);

    req.body.taxPercent = typeof req.body.taxPercent == "string" ? JSON.parse(req.body.taxPercent) : req.body.taxPercent;
    req.body.deliveryDateTime = typeof req.body.deliveryDateTime == "string" ? JSON.parse(req.body.deliveryDateTime) : req.body.deliveryDateTime;
    req.body.destination_account = typeof req.body.destination_account == "string" ? JSON.parse(req.body.destination_account) : req.body.destination_account;
    req.body.taxAmount = typeof req.body.taxAmount == "string" ? JSON.parse(req.body.taxAmount) : req.body.taxAmount;
    req.body.discountApplied = typeof req.body.discountApplied == "string" ? JSON.parse(req.body.discountApplied) : req.body.discountApplied;

    successlog.info(`create_charge_multiple :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'GET',
        url: B2B_API_ENDPOINT + 'accounts?access_token=' + req.body.accessToken + '&filter={"where":{"id":"' + req.body.accountId + '"}}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    };

    request(options, function (error, response, body) {
        if (error) {
            console.log('Cannot Find User Account linked with Access Token')
            res.status(400).send({"message": JSON.stringify(error), "status":error}).end()
        } else {
            if (JSON.parse(body).error) {
                res.status(200).send({ "message": JSON.parse(body).error.message, "status": "error" }).end();
            } else {
                console.log('User details received');
                var options = {
                    method: 'GET',
                    url: B2B_API_ENDPOINT + 'deliverylocations?access_token=' + req.body.accessToken + '&filter={"where":{"accountId":"' + req.body.accountId + '"}}',
                    headers: { 'content-type': 'application/x-www-form-urlencoded' }
                };

                request(options, function (error, response, body) {
                    if (error) {
                        res.status(400).send({"message": JSON.stringify(error), "status":error}).end()
                    } else {
                        if (JSON.parse(body).error) {
                            res.send({ "message": JSON.parse(body).error.message, "status": "error" }).end();
                        } else {
                            console.log('Delivery location received');
                            deliveryLocation = JSON.parse(body)[0];

                            //FETCH SHOPPING CART
                            var options = {
                                method: 'GET',
                                url: B2B_API_ENDPOINT + 'shoppingcarts?filter={"where":{"accountId":"' + req.body.accountId + '","state":0},"include":["offer"]}',
                                headers: { 'content-type': 'application/x-www-form-urlencoded' }
                            };

                            request(options, function (error, response, body) {
                                if (error) {
                                    successlog.info(`create_charge_multiple :: USERS SHOPPING CART :: error :: ${JSON.stringify(error)}`);
                                    successlog.info(`create_charge_multiple :: USERS SHOPPING CART :: error :: ${JSON.stringify(body)}`);
                                    res.status(400).send({'Error fetching shopping cart.':error}).end();
                                } else {
                                    successlog.info(`create_charge_multiple :: USERS SHOPPING CART :: ${JSON.stringify(body)}`);
                                    console.log(JSON.stringify(body));
                                    shoppingCartBody = JSON.parse(body);
                                    let establishmentArray = [];
                                    let merchantOneTotal = 0;
                                    let merchantTwoTotal = 0;

                                    let merchantOneDeliveryDate = '';
                                    let merchantTwoDeliveryDate = '';


                                    shoppingCartBody.forEach(function (item) {
                                        tmpTotalPrice += item.offer.salePrice * item.quantity;
                                        if (typeof establishmentArray[item.establishmentId] != "undefined") {
                                            // if (establishmentArray.indexOf(item.establishmentId) > -1) {
                                            establishmentArray[item.establishmentId].push(item);
                                        } else {
                                            establishmentArray[item.establishmentId] = [];
                                            establishmentArray[item.establishmentId].push(item);
                                        }
                                    });

                                    let merchantOneEstablishmentId = '';
                                    let merchantTwoEstablishmentId = '';

                                    for (let i in establishmentArray) {
                                        let subtotal = 0;
                                        let totalPriceForTax = 0;
                                        let establishmentId = "";
                                        establishmentArray[i].forEach((item) => {
                                            establishmentId = item.offer.establishmentId;
                                            subtotal += (item.offer.salePrice) * item.quantity;
                                        })
                                        if (req.body.discountApplied.est1.establishmentId == establishmentId) {
                                            merchantOneEstablishmentId = establishmentId;
                                            merchantOneDeliveryDate = req.body.deliveryDateTime.deliveryDate;
                                            merchantOneTotal = parseFloat((parseFloat(subtotal) - parseFloat(req.body.discountApplied.est1.discountAmount) + parseFloat(req.body.taxAmount[establishmentId])).toFixed(2));
                                        } else if (req.body.discountApplied.est2.establishmentId == establishmentId) {
                                            merchantTwoDeliveryDate = req.body.deliveryDateTime.otherDeliveryDate;
                                            merchantTwoEstablishmentId = establishmentId;
                                            merchantTwoTotal = parseFloat((parseFloat(subtotal) - parseFloat(req.body.discountApplied.est2.discountAmount) + parseFloat(req.body.taxAmount[establishmentId])).toFixed(2));
                                        }
                                    }

                                    shoppingCartBody.forEach((element) => {
                                        if (element.establishmentId === merchantOneEstablishmentId) {
                                            shoppingCartBody1.push(element);
                                        } else if (element.establishmentId === merchantTwoEstablishmentId) {
                                            shoppingCartBody2.push(element);
                                        }
                                    });

                                    let bothMerchantTotal = parseFloat((merchantOneTotal + merchantTwoTotal).toFixed(2));
                                    let merchantOnePercent = parseFloat(((merchantOneTotal / bothMerchantTotal) * 100).toFixed(2));
                                    let merchantTwoPercent = parseFloat(((merchantTwoTotal / bothMerchantTotal) * 100).toFixed(2));

                                    let tipForMerchantOne = 0;
                                    let tipForMerchantTwo = 0;
                                    if (!req.body.isShipping[merchantOneEstablishmentId]) {
                                        tipForMerchantOne = parseFloat((req.body.tipAmount * (merchantOnePercent / 100)).toFixed(2));
                                        tipForMerchantTwo = parseFloat((req.body.tipAmount * (merchantTwoPercent / 100)).toFixed(2));
                                    } else {
                                        tipForMerchantOne = 0;
                                        tipForMerchantTwo = parseFloat((req.body.tipAmount * (100 / 100)).toFixed(2));
                                    }
                                    

                                    let finalAmountForMerchantOne = parseInt(parseFloat((tipForMerchantOne + merchantOneTotal).toFixed(2)) * 100);
                                    let finalAmountForMerchantTwo = parseInt(parseFloat((tipForMerchantTwo + merchantTwoTotal).toFixed(2)) * 100);

                                    let finalTotalPrice = parseInt(finalAmountForMerchantOne + finalAmountForMerchantTwo);

                                    let discountApplied1 = {};
                                    let discountApplied2 = {};
                                    let promoCode1 = '';
                                    let promoCode2 = '';

                                    if (req.body.discountApplied.est1.establishmentId === merchantOneEstablishmentId) {
                                        discountApplied1 = req.body.discountApplied.est1;
                                        promoCode1 = req.body.promoCode.est1;
                                    } else if (req.body.discountApplied.est2.establishmentId === merchantOneEstablishmentId) {
                                        discountApplied1 = req.body.discountApplied.est2;
                                        promoCode2 = req.body.promoCode.est2;
                                    }

                                    if (req.body.discountApplied.est1.establishmentId === merchantTwoEstablishmentId) {
                                        discountApplied2 = req.body.discountApplied.est1;
                                    } else if (req.body.discountApplied.est2.establishmentId === merchantTwoEstablishmentId) {
                                        discountApplied2 = req.body.discountApplied.est2;
                                    }

                                    console.log('Establishment ID 1',merchantOneEstablishmentId);
                                    console.log('Establishment Id 2', merchantTwoEstablishmentId);
                                    console.log("shoppingCartBody", shoppingCartBody)
                                    console.log("shoppingCartBody1", shoppingCartBody1)
                                    console.log("shoppingCartBody2", shoppingCartBody2)
                                    console.log("establishmentArray", establishmentArray)
                                    console.log("merchantOneTotal", merchantOneTotal)
                                    console.log("merchantTwoTotal", merchantTwoTotal)
                                    console.log("merchantOnePercent", merchantOnePercent)
                                    console.log("merchantTwoPercent", merchantTwoPercent)
                                    console.log("tipForMerchantOne", tipForMerchantOne)
                                    console.log("tipForMerchantTwo", tipForMerchantTwo)
                                    console.log("finalAmountForMerchantOne", finalAmountForMerchantOne)
                                    console.log("finalAmountForMerchantTwo", finalAmountForMerchantTwo)
                                    console.log("finalTotalPrice", finalTotalPrice)

                                    successlog.info(`++++++++++++++++++++++++++++++++ Amount from web ${req.body.amount}`);
                                    successlog.info(`++++++++++++++++++++++++++++++++ finalTotalPrice ${finalTotalPrice}`);

                                    if (finalAmountForMerchantOne < 100 || finalAmountForMerchantTwo < 100) {
                                        res.send({ "discountUsed": "Total Amount is less than $1.00. Order cannot be placed", "status": "error" }).end();
                                    } else {
                                        //================================== Stripe Auth =====================================
                                        //Auth 1st Merchant
                                        let paymentInfo_1 = {
                                            amount: finalAmountForMerchantOne.toString(),
                                            currency: "usd",
                                            customer: req.body.customerId,
                                            email: req.body.email,
                                            source: req.body.source,
                                            destination_account: req.body.destination_account[merchantOneEstablishmentId],
                                            accountId: req.body.accountId,
                                            taxAmount: req.body.taxAmount[merchantOneEstablishmentId],
                                            taxPercent: req.body.taxPercent[merchantOneEstablishmentId],
                                            tipAmount: tipForMerchantOne,
                                            discountApplied: discountApplied1,
                                            promoCode: promoCode1,
                                            deliveryFee: req.body.deliveryFee[merchantOneEstablishmentId],
                                            isShipping: req.body.isShipping[merchantOneEstablishmentId],
                                            shippingFee: req.body.shippingFee[merchantOneEstablishmentId],
                                            orderForOther: req.body.orderForOther,
                                            orderPhoneNumber: req.body.orderPhoneNumber,
                                            establishmentId: merchantOneEstablishmentId,
                                            deliveryDateTime: merchantOneDeliveryDate,
                                            deliveryInstructions: req.body.deliveryInstructions ? req.body.deliveryInstructions : '',
                                            suitNumber: req.body.suitNumber,
                                            ipAddress: ipAddress,
                                            browserAcceptLanguage: browserAcceptLanguage,
                                            userAgent: userAgent,
                                            accessToken: req.body.accessToken,
                                            shoppingCart: shoppingCartBody1,
                                            deliveryLocation: deliveryLocation,
                                            multiple: true,
                                            count: 1,
                                        }

                                        let paymentInfo_2 = {
                                            amount: finalAmountForMerchantTwo.toString(),
                                            currency: "usd",
                                            customer: req.body.customerId,
                                            source: req.body.source,
                                            email: req.body.email,
                                            destination_account: req.body.destination_account[merchantTwoEstablishmentId],
                                            accountId: req.body.accountId,
                                            taxAmount: req.body.taxAmount[merchantTwoEstablishmentId],
                                            taxPercent: req.body.taxPercent[merchantTwoEstablishmentId],
                                            tipAmount: tipForMerchantTwo,
                                            discountApplied: discountApplied2,
                                            promoCode: promoCode2,
                                            deliveryFee: req.body.deliveryFee[merchantTwoEstablishmentId],
                                            isShipping: req.body.isShipping[merchantTwoEstablishmentId],
                                            shippingFee: req.body.shippingFee[merchantTwoEstablishmentId],
                                            orderForOther: req.body.orderForOther,
                                            orderPhoneNumber: req.body.orderPhoneNumber,
                                            establishmentId: merchantTwoEstablishmentId,
                                            deliveryDateTime: merchantTwoDeliveryDate,
                                            deliveryInstructions: req.body.deliveryInstructions ? req.body.deliveryInstructions : '',
                                            suitNumber: req.body.suitNumber,
                                            ipAddress: ipAddress,
                                            browserAcceptLanguage: browserAcceptLanguage,
                                            userAgent: userAgent,
                                            accessToken: req.body.accessToken,
                                            shoppingCart: shoppingCartBody2,
                                            deliveryLocation: deliveryLocation,
                                            multiple: true,
                                            count: 2,
                                        }

                                        console.log(paymentInfo_1);
                                        console.log(paymentInfo_2);

                                        if (req.body.paymentConfirmed && req.body.paymentConfirmed2) {
                                            if (req.body.paymentIntent && req.body.paymentIntent2) {
                                                // Check maxmind score of 1st
                                                console.log('Check maxmind score for 1')
                                                paymentInfo_1.status = 0;
                                                paymentInfo_2.status = 0;
                                                if (req.body.paymentIntent.destination_account === paymentInfo_1.destination_account) {
                                                    paymentInfo_1.paymentIntent = req.body.paymentIntent;
                                                    paymentInfo_2.paymentIntent = req.body.paymentIntent2;
                                                } else {
                                                    paymentInfo_2.paymentIntent = req.body.paymentIntent;
                                                    paymentInfo_1.paymentIntent = req.body.paymentIntent2;
                                                }
                                                if (process.env.NODE_ENV === 'production') {
                                                    
                                                    // Check maxmind for paymentInfo_1
                                                    var options = {
                                                        url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                                        method: 'POST',
                                                        headers: { 'content-type': 'application/json' },
                                                        json: true,
                                                        body: {
                                                            orderInfo: paymentInfo_1
                                                        }
                                                    };
    
                                                    request(options, function(error, response, body) {
                                                        if (error) {
                                                            console.log(error);
                                                            res.status(400).send(error);
                                                        } else if (response.statusCode === 200) {
                                                            let decision_1 = body.decision;
                                                            console.log('Maxmind score for paymentIntent '+paymentInfo_1.paymentIntent.id+' is ', decision_1);
                                                            
                                                            // if (decision_1 != -1 && decision_1 > 0 && decision_1 <= maxmindThreshold) {
                                                                if (decision_1 === 'pass') {
                                                                
                                                                // if score requirement is satisfied, proceed to check score for 2nd    
                                                                var options = {
                                                                    url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                                                    method: 'POST',
                                                                    headers: { 'content-type': 'application/json' },
                                                                    json: true,
                                                                    body: {
                                                                        orderInfo: paymentInfo_2
                                                                    }
                                                                };
    
                                                                request(options, function(error, response, body_2) {
                                                                    if (error) {
                                                                        cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_1);
                                                                    } else if (response.statusCode === 200) {
                                                                        let decision_2 = body_2.decision;
                                                                        console.log('Maxmind score for paymentIntent '+paymentInfo_2.paymentIntent.id+' is ', decision_2);
                                                                        // if (decision_2 != -1 && decision_2 > 0 && decision_2 <= maxmindThreshold) {
                                                                        if (decision_2 === 'pass') {
                                                                            // if score requirement is satisifed for 2nd, then proceed for further transaction
                                                                            if (req.body.paymentIntent.destination_account === paymentInfo_1.destination_account) {
                                                                                paymentInfo_1.riskScore = decision_1;
                                                                                paymentInfo_1.decision = body.decision;
                                                                                paymentInfo_1.noFraudId = body.id;
                                                                                // paymentInfo_2.riskScore = decision_2;
                                                                                paymentInfo_2.decision = body_2.decision;
                                                                                paymentInfo_2.noFraudId = body_2.id;
                                                                                paymentInfo_2.maxmindRiskScore = body_2.maxmindRiskScore;
                                                                                
                                                                                mutlipleTransactionCallBack(res, req.body.paymentIntent, '', paymentInfo_1, req.body.paymentIntent2, '', paymentInfo_2);
                                                                            } else {
                                                                                paymentInfo_1.riskScore = decision_2;
                                                                                paymentInfo_2.riskScore = decision_1;
                                                                                mutlipleTransactionCallBack(res, req.body.paymentIntent2, '', paymentInfo_1, req.body.paymentIntent, '', paymentInfo_2);
                                                                            }
                                                                        } else {
                                                                            cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_2);
                                                                        }
                                                                    } else {
                                                                        cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_1);
                                                                    }
                                                                    // else if score requirement is not satisfied then, cancel 2nd
                                                                    // cancel 1st
                                                                })
                                                                
                                                            } else {
                                                                // else if score requirement is not satisfied, then cancel 1st
                                                                cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_1);
                                                            }
                                                        } else {
                                                            console.log(error);
                                                            res.status(400).send({'status': 'error', 'message': 'Error'});
                                                        }
                                                        
                                                    });
                                                } else {
                                                    paymentInfo_1.decision = 'pass';
                                                    paymentInfo_1.noFraudId = 123;
                                                    paymentInfo_1.maxmindRiskScore = 0.1;
                                                    paymentInfo_2.decision = 'pass';
                                                    paymentInfo_2.noFraudId = 1234;
                                                    paymentInfo_2.maxmindRiskScore = 0.1;
                                                    mutlipleTransactionCallBack(res, req.body.paymentIntent, '', paymentInfo_1, req.body.paymentIntent2, '', paymentInfo_2);
                                                }
                                                
                                            } else {
                                                if (req.body.paymentIntent.destination_account === paymentInfo_1.destination_account) {
                                                    mutlipleTransactionCallBack(res, '', req.body.paymentIntentError, paymentInfo_1, '', req.body.paymentIntent2Error, paymentInfo_2);
                                                } else {
                                                    mutlipleTransactionCallBack(res, '', req.body.paymentIntent2Error, paymentInfo_1, '', req.body.paymentIntentError, paymentInfo_2);
                                                }
                                            }
                                            
                                        } else {
                                            try {
                                                // Clone1: Get Clone Payment Method for PaymenInfo 1
                                                cloningPaymentMethod(paymentInfo_1)
                                                .then((source_1) => {
                                                    console.log('Cloning Source Result', source_1);
                                                    paymentInfo_1.payment_method = source_1.id;
                                                    // Create Payment Intenet for PaymentInfo1 with Payment Method from Clone 1
                                                    createPaymentIntents(paymentInfo_1)
                                                    .then((charge1) => {
                                                        console.log('Payment Intend for Establishment 1 was successfull', merchantOneEstablishmentId);
                                                        console.log(charge1);
                                                        if (charge1.status === 'requires_confirmation') {
                                                            // Clone2: Get Clone Payment Method for PaymentInfo2
                                                            cloningPaymentMethod(paymentInfo_2)
                                                            .then((source_2) => {
                                                                console.log('Cloning Source Result', source_2);
                                                                paymentInfo_2.payment_method = source_2.id;
                                                                // Create Payment Intents for merchant 2
                                                                createPaymentIntents(paymentInfo_2)
                                                                .then((charge2) => {
                                                                    console.log('Payment Intend for Establishment 2 was successfull', merchantTwoEstablishmentId);
                                                                    console.log(charge2);
                                                                    if (charge1.status === 'requires_confirmation' && charge2.status === 'requires_confirmation') {
                                                                        charge1.destination_account = paymentInfo_1.destination_account;
                                                                        charge2.destination_account = paymentInfo_2.destination_account;
                                                                        res.status(200).send({charge1: charge1, charge2: charge2});
                                                                    } else {
                                                                        cancelPaymentIntents(charge1)
                                                                        .then((charge) => {
                                                                            let error = {
                                                                                message: 'Server error, please try again.'
                                                                            }
                                                                            res.status(402).send(error);
                                                                        })
                                                                        .catch((err) => {
                                                                            let error = {
                                                                                message: 'Server error, please try again.'
                                                                            }
                                                                            res.status(402).send(error);
                                                                        })
                                                                        
                                                                    }
                                                                    
                                                                })
                                                                .catch((error) => {
                                                                    console.log('Error for Establishment 2', merchantTwoEstablishmentId);
                                                                    console.log(error);
                                                                    if (charge1.status === "requires_payment_method" || charge1.status === "requires_capture" || charge1.status === "requires_confirmation"
                                                                    || charge1.status === "requires_action" || charge1.status === "requires_source" || charge1.status === "requires_source_action") {
                                                                        // Refund for charge 1
                                                                        cancelPaymentIntents(charge1)
                                                                        .then((charge) => {
                                                                            mutlipleTransactionCallBack(res, '', error, paymentInfo_1, '', error, paymentInfo_2);
                                                                        })
                                                                        .catch((err) => {
                                                                            mutlipleTransactionCallBack(res, '', error, paymentInfo_1, '', error, paymentInfo_2);
                                                                        })
                                                                    }  
                                                                })
                                                            })
                                                            .catch((err) => {
                                                                // console.log('Cloning Source Failed');
                                                                console.log('Cloning Payment Method failed for Payment Info 2')
                                                                console.log(err);
                                                                cancelPaymentIntents(charge1)
                                                                .then((charge) => {
                                                                    mutlipleTransactionCallBack(res, '', err, paymentInfo_1, '', err, paymentInfo_2);
                                                                })
                                                                .catch((err) => {
                                                                    mutlipleTransactionCallBack(res, '', err, paymentInfo_1, '', err, paymentInfo_2);
                                                                })
                                                                // res.status(err.statusCode).send(JSON.stringify(err)).end();
                                                            });
                                                        } else {
                                                            let error = {
                                                                message: 'Server error, please try again.'
                                                            }
                                                            res.status(402).send(error);
                                                        }
                                                        
                                                        
                                                    })
                                                    .catch((error) => {
                                                        console.log('Error for Establishment 1', merchantOneEstablishmentId);
                                                        // Send Transactions for Merchant1
                                                        // Send Transactions for Merchant2 with error of Merchant 1
                                                        mutlipleTransactionCallBack(res, '', error, paymentInfo_1, '', error, paymentInfo_2);
                                                    })
                                                })
                                                .catch((err) => {
                                                    console.log('Cloning Payment Method failed for Payment Info 1')
                                                    console.log(err);
                                                    res.status(err.statusCode).send(JSON.stringify(err)).end();
                                                });
                                            } catch (err) {
                                                console.log('Error');
                                                res.status(200).send({"mesage: ":'Error fetching details'}).end();
                                            }
                                        }

                                        
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });

});

router.post('/update_charge_b2b', function (req, res, next) {

});

router.post('/update_charge_multiple', function(req, res, next) {

});

/**
 * Stripe PaymentIntents for CorpOrder
 */
router.post('/create_corp_charge', function (req, res, next) {
  console.log('Create Corp Charge', req.body);
    const corpOrderId = req.body.corpOrderId;
    const accountId = req.body.accountId;
    const destinationAccount = req.body.destinationAccount;
    const cardSourceId = req.body.cardSourceId;
    const accessToken = req.body.accessToken;
    const paymentToken = req.body.paymentToken;
    const orderAmount = req.body.amount;
    const bartenderAmount = req.body.bartender;
    let ipAddress;
    let userAgent;
    let browserAcceptLanguage;

    // console.log(req);
    if (req._remoteAddress.substr(0,7) === "::ffff:") {
        ipAddress = req._remoteAddress.substr(7);
    }
    // Comment these line when using on dev server and prod server
    if (process.env.NODE_ENV === 'development') {
        // ipAddress = '103.255.182.250';
    }
    

    Object.entries(req.headers).forEach((key,res) => {
        if (key[0] === "user-agent") {
            userAgent = key[1];
        } else if (key[0] === "accept-language") {
            browserAcceptLanguage = key[1];
        }
    })

    var options = {
        method: 'GET',
        url: `${B2B_API_ENDPOINT}corporders/getCorpOrder?accountId=${accountId}`,
        headers: {'content-type': 'application/x-www-form-urlencoded'}
    };

    request(options, function(error, response, body) {
        if (error) {

        } else {
            successlog.info(`create_corp_charge :: CORP ORDER DATA :: ${JSON.stringify(body)}`);

            let paymentInfo = {
                accessToken: accessToken,
                amount: orderAmount,
                currency: 'usd',
                customer: paymentToken,
                source: cardSourceId,
                destination_account: destinationAccount,
                accountId: accountId,
                corpOrderId: corpOrderId,
                browserAcceptLanguage: browserAcceptLanguage,
                ipAddress: ipAddress,
                userAgent: userAgent,
                shoppingCart: JSON.parse(body),
                taxAmount: 0,
                bartenderAmount: bartenderAmount,
                count: 0,
            }


            if (paymentInfo.bartenderAmount) {
                paymentInfo.amount = orderAmount - body.bartender;
                if (req.body.paymentConfirmed && req.body.bartenderPaymentConfirmed) {
                    if (req.body.paymentIntent && req.body.paymentIntent2) {
                        // Check maxmind score of 1st
                        console.log('Check maxmind score for 1')
                        paymentInfo_1.status = 0;
                        paymentInfo_2.status = 0;
                        if (req.body.paymentIntent.destination_account === paymentInfo_1.destination_account) {
                            paymentInfo_1.paymentIntent = req.body.paymentIntent;
                            paymentInfo_2.paymentIntent = req.body.paymentIntent2;
                        } else {
                            paymentInfo_2.paymentIntent = req.body.paymentIntent;
                            paymentInfo_1.paymentIntent = req.body.paymentIntent2;
                        }
                        if (process.env.NODE_ENV === 'production') {
                            
                            // Check maxmind for paymentInfo_1
                            var options = {
                                url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                method: 'POST',
                                headers: { 'content-type': 'application/json' },
                                json: true,
                                body: {
                                    orderInfo: paymentInfo_1
                                }
                            };

                            request(options, function(error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(400).send(error);
                                } else if (response.statusCode === 200) {
                                    let decision_1 = body.decision;
                                    console.log('Maxmind score for paymentIntent '+paymentInfo_1.paymentIntent.id+' is ', decision_1);
                                    
                                    // if (decision_1 != -1 && decision_1 > 0 && decision_1 <= maxmindThreshold) {
                                        if (decision_1 === 'pass') {
                                        
                                        // if score requirement is satisfied, proceed to check score for 2nd    
                                        var options = {
                                            url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                            method: 'POST',
                                            headers: { 'content-type': 'application/json' },
                                            json: true,
                                            body: {
                                                orderInfo: paymentInfo_2
                                            }
                                        };

                                        request(options, function(error, response, body_2) {
                                            if (error) {
                                                cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_1);
                                            } else if (response.statusCode === 200) {
                                                let decision_2 = body_2.decision;
                                                console.log('Maxmind score for paymentIntent '+paymentInfo_2.paymentIntent.id+' is ', decision_2);
                                                // if (decision_2 != -1 && decision_2 > 0 && decision_2 <= maxmindThreshold) {
                                                if (decision_2 === 'pass') {
                                                    // if score requirement is satisifed for 2nd, then proceed for further transaction
                                                    if (req.body.paymentIntent.destination_account === paymentInfo_1.destination_account) {
                                                        paymentInfo_1.riskScore = decision_1;
                                                        paymentInfo_1.decision = body.decision;
                                                        paymentInfo_1.noFraudId = body.id;
                                                        // paymentInfo_2.riskScore = decision_2;
                                                        paymentInfo_2.decision = body_2.decision;
                                                        paymentInfo_2.noFraudId = body_2.id;
                                                        paymentInfo_2.maxmindRiskScore = body_2.maxmindRiskScore;
                                                        
                                                        mutlipleTransactionCallBack(res, req.body.paymentIntent, '', paymentInfo_1, req.body.paymentIntent2, '', paymentInfo_2, true);
                                                    } else {
                                                        paymentInfo_1.riskScore = decision_2;
                                                        paymentInfo_2.riskScore = decision_1;
                                                        mutlipleTransactionCallBack(res, req.body.paymentIntent2, '', paymentInfo_1, req.body.paymentIntent, '', paymentInfo_2, true);
                                                    }
                                                } else {
                                                    cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_2);
                                                }
                                            } else {
                                                cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_1);
                                            }
                                            // else if score requirement is not satisfied then, cancel 2nd
                                            // cancel 1st
                                        })
                                        
                                    } else {
                                        // else if score requirement is not satisfied, then cancel 1st
                                        cancelingMultipleOrder(res, paymentInfo_1, decision_1, paymentInfo_2, decision_1);
                                    }
                                } else {
                                    console.log(error);
                                    res.status(400).send({'status': 'error', 'message': 'Error'});
                                }
                                
                            });
                        } else {
                            paymentInfo_1.decision = 'pass';
                            paymentInfo_1.noFraudId = 123;
                            paymentInfo_1.maxmindRiskScore = 0.1;
                            paymentInfo_2.decision = 'pass';
                            paymentInfo_2.noFraudId = 1234;
                            paymentInfo_2.maxmindRiskScore = 0.1;
                            mutlipleTransactionCallBack(res, req.body.paymentIntent, '', paymentInfo_1, req.body.paymentIntent2, '', paymentInfo_2, true);
                        }
                    } else {
                        if (req.body.paymentIntent.destination_account === paymentInfo_1.destination_account) {
                            mutlipleTransactionCallBack(res, '', req.body.paymentIntentError, paymentInfo_1, '', req.body.paymentIntent2Error, paymentInfo_2, true);
                        } else {
                            mutlipleTransactionCallBack(res, '', req.body.paymentIntent2Error, paymentInfo_1, '', req.body.paymentIntentError, paymentInfo_2, true);
                        }
                    }
                } else {
                    try {
                        cloningPaymentMethod(paymentInfo)
                        .then((source) => {
                            console.log('Cloning Source Result', source);
                            paymentInfo.payment_method = source.id;
                                    createPaymentIntents(paymentInfo)
                                    .then(function(charge1) {
                                        console.log(charge1);
                                        console.log('CREATE PAYMENT INTENTS')
                                        if (charge1.status === 'requires_confirmation' || charge1.status === 'requires_capture') {
                                            paymentInfo.bevviAmount = paymentInfo.bartenderAmount;
                                            cloningPaymentMethodForBevvi(paymentInfo)
                                            .then((source2) => {
                                                paymentInfo.payment_method_bevvi = paymentInfo.source;
                                                createPaymentIntentsForBevvi(paymentInfo)
                                                .then((charge2) => {
                                                    console.log(charge2);
                                                    if (charge1.status === 'requires_confirmation' && charge2.status === 'required_confirmation') {
                                                        charge1.destination_account = paymentInfo.destination_account;
                                                        charge2.destination_account = charge2.merchantAccountId;
                                                        res.status(200).send({charge1, charge2});
                                                    } else if (charge1.status === 'requires_capture' && charge2.status === 'requires_capture') { 
                                                        mutlipleTransactionCallBack(res, charge1, {}, paymentInfo, charge2, {}, paymentInfo2, {}, true);
                                                    }
                                                })
                                            })
                                        } else {
                                            res.status(400).send({"status":"error", "message":'Payment cannot process'});
                                        }
                                    }).catch(function(err) {
                                        console.log('Error Creating Payment Intent');
                                        console.log(err);
                                        transactionOrder(res, '', err, paymentInfo, true, chargeCallBack);
                                    });
                        })
                        .catch((err) => {
                            // console.log('Cloning Source Failed');
                            console.log('Cloning Payment Method failed')
                            console.log(err);
                            res.status(err.statusCode).send(JSON.stringify(err)).end();
                        });
                        
                    } catch (err) {
                        console.log('Error');
                        res.status(200).send({"mesage: ":'Error fetching details'}).end();
                    }
                }
            } else {
                if (req.body.paymentConfirmed) {
                    if (req.body.paymentIntent) {
                        paymentInfo.status = 0;
                        paymentInfo.paymentIntent = req.body.paymentIntent;
                        if (process.env.NODE_ENV === 'production') {
                            console.log('Send Data to nofraud');
    
                            var options = {
                                url: B2B_API_ENDPOINT + 'bevviutils/nofraud',
                                method: 'POST',
                                headers: { 'content-type': 'application/json' },
                                json: true,
                                body: {
                                    orderInfo: paymentInfo
                                }
                            };
    
                            request(options, function (error, response, body) {
                                if (error) {
                                    console.log(error);
                                    res.status(400).send(error);
                                } else if (response.statusCode === 200) {
                                    // let riskScore = body.maxmindRiskScore;
                                    // console.log('Maxmind Score is ', riskScore)
                                    let decision = body.decision;
                                    // if (riskScore != -1 && riskScore > 0 && riskScore <= maxmindThreshold) {
                                    if (decision === 'pass') {
                                        paymentInfo.riskScore = decision;
                                        paymentInfo.decision = body.decision;
                                        paymentInfo.noFraudId = body.id;
                                        // paymentInfo.maxmindRiskScore = body.maxmindRiskScore;
                                        transactionOrder(res, req.body.paymentIntent, '', paymentInfo, true, chargeCallBack);
                                    } else {
                                        cancelingOrder(res, paymentInfo, decision);
                                    }
                                } else {
                                    res.status(400).send({'status': 'error', 'message': 'Error'});
                                }
                            });
                        } else {
                            console.log('Send Data to nofraud');
                            paymentInfo.decision = "pass";
                            paymentInfo.noFraudId = 123;
                            paymentInfo.maxmindRiskScore = 0.1;
                            transactionOrder(res, req.body.paymentIntent, '', paymentInfo, true, chargeCallBack);
                        }
                    } else {
                        transactionOrder(res, '', req.body.paymentIntentError, '', paymentInfo, true, chargeCallBack);
                    }
                } else {
                    try {
                        cloningPaymentMethod(paymentInfo)
                        .then((source) => {
                            console.log('Cloning Source Result', source);
                            paymentInfo.payment_method = source.id;
                                    createPaymentIntents(paymentInfo)
                                    .then(function(charge) {
                                        console.log(charge);
                                        console.log('CREATE PAYMENT INTENTS')
                                        if (charge.status === 'requires_confirmation') {
                                            res.status(200).send(charge);
                                        } else if (charge.status === 'requires_capture') {
                                            transactionOrder(res, charge, '', paymentInfo, true, chargeCallBack);
                                        } else {
                                            res.status(400).send({"status":"error", "message":'Payment cannot process'});
                                        }
                                    }).catch(function(err) {
                                        console.log('Error Creating Payment Intent');
                                        console.log(err);
                                        transactionOrder(res, '', err, paymentInfo, true, chargeCallBack);
                                    });
                        })
                        .catch((err) => {
                            // console.log('Cloning Source Failed');
                            console.log('Cloning Payment Method failed')
                            console.log(err);
                            res.status(err.statusCode).send(JSON.stringify(err)).end();
                        });
                        
                    } catch (err) {
                        console.log('Error');
                        res.status(200).send({"mesage: ":'Error fetching details'}).end();
                    }
                }
            }
        }
    });
});

/**
 * Stripe Capture Charge Functions
 */

router.post('/capture_charge_b2b', function (req, res, next) {
    console.log("capture_charge_b2b req.body", req.body);
    successlog.info(`capture_charge_b2b :: REQUEST BODY :: ${JSON.stringify(req.body)}`);
    var order_id = req.body.order_id;
    var access_token = req.body.access_token;
    var action = req.body.action;
    return captureCharge(order_id, access_token, action, false, req, res)
});

router.post('/capture_corp_charge_b2b', function (req, res, next) {
    console.log("capture_charge_b2b req.body", req.body);
    successlog.info(`capture_charge_b2b :: REQUEST BODY :: ${JSON.stringify(req.body)}`);
    var order_id = req.body.order_id;
    var access_token = req.body.access_token;
    var action = req.body.action;
    return captureCharge(order_id, access_token, action, true, req, res)
});

function captureCharge(order_id, access_token, action, isCorpOrder=false, req, res) {
    var charge_id = "";
    var transaction_detail = "";
    if (typeof order_id == "undefined" || order_id == "") {
        res.send({ "message": "Order Id is required", "status": "error" });
        res.end();
    } else if (typeof action == "undefined" || action == "") {
        res.send({ "message": "Action is required", "status": "error" });
        res.end();
    } else if (typeof access_token == "undefined" || access_token == "") {
        res.send({ "message": "Access Token is required", "status": "error" });
        res.end();
    } else {
        var url = 'transactions?filter={"where":{"orderId":"' + order_id + '"},"include":"order"}&access_token=' + access_token;
        if (isCorpOrder) {
            url = 'transactions?filter={"where":{"corpOrderId":"' + order_id + '"},"include":"corporder"}&access_token=' + access_token;
        }
        console.log('URL', B2B_API_ENDPOINT + url);
        //FETCH TRANSACTION
        var options = {
            method: 'GET',
            url: B2B_API_ENDPOINT + url,
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        };

        request(options, async function (error, response, body) {
            if (error) {
                successlog.info(`capture_charge_b2b :: TRANSACTION FOR CURRENT ORDER :: error :: ${JSON.stringify(error)}`);
                successlog.info(`capture_charge_b2b :: TRANSACTION FOR CURRENT ORDER :: error :: ${JSON.stringify(body)}`);
                res.status(200).send({ "message": "Error Occured", "status": "error" }).end();
            } else {
                successlog.info(`capture_charge_b2b :: TRANSACTION FOR CURRENT ORDER :: ${JSON.stringify(body)}`);
                if (error) {
                    res.status(200).send({ "message": "Error Occured", "status": "error" }).end();
                } else {
                    if (JSON.parse(body).error) {
                        res.status(200).send({ "message": JSON.parse(body).error.message, "status": "error", "status_code": "SESSION_EXPIRED" }).end();
                    } else {
                        $complete_flag = false;
                        $est_flag = false;
                        $tax_flag = false;
                        $single_flag = true;
                        let transaction_arr =JSON.parse(body) 
                        if (transaction_arr.length == 1) {
                            $tax_flag = true;
                        } else if (!isCorpOrder) {
                            $single_flag = false;
                        }
                        
                        let originalOrderAmount = 0;
                        for (let [index, transaction_detail] of transaction_arr.entries()) {
                            console.log('Request for ', transaction_detail);
                            // transaction_detail = JSON.parse(body)[0];
                            charge_id = transaction_detail.chargeId;
                            let orderAmount = 0
                            if (isCorpOrder) {
                                orderAmount = transaction_detail.corporder.storeTotal;
                            } else if (!$single_flag) {
                                orderAmount =  transaction_detail.totalAmt
                            } else {
                                orderAmount = transaction_detail.order.totalAmount
                            }
                            originalOrderAmount = originalOrderAmount + orderAmount;
                            // orderAmount = isCorpOrder ? transaction_detail.corporder.storeTotal : transaction_detail.order.totalAmount;
                            let merchantAccountId = transaction_detail.merchantAccountId;
                            let type = 'non-bevvi';
                            
                            if (merchantAccountId == BEVVI_STRIPE_ACC) {
                                type = 'bevvi'
                            }
                            if (isCorpOrder && type == 'bevvi') {
                                orderAmount = transaction_detail.corporder.bevviTotal;
                            }
                            let statusType = "CARD_AUTHORIZED"
                            if (action === 'reject' || action === 'partial-refund') {
                                statusType = "CARD_CHARGED"
                            }

                            console.log('ORDER AMOUNT', orderAmount)
                            console.log('merchantAccountId', merchantAccountId)
                            console.log('Type for order', type)
                            console.log('Transaction Detail Status', transaction_detail.status)

                            console.log(action == "reject" && transaction_detail.status === statusType)

                            if (action == "accept" && transaction_detail.status === statusType) {
                                // capturePaymentIntents(charge_id, orderAmount, merchantAccountId)
                                await sendCapturePaymentIntent(type, {charge_id, orderAmount, merchantAccountId})
                                .then((capture) => {
                                    console.log(`For order ${index} `, capture);
                                    if (capture.status === 'succeeded') {
                                        let charge = capture;
                                        successlog.info(`capture_charge_b2b :: CAPTURE CHARGE :: ${JSON.stringify(charge)}`);
    
                                        //CREATE TRANSACTION
                                        var options = {
                                            method: 'POST',
                                            url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                            headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                            form: {
                                                "accountId": transaction_detail.accountId,
                                                "bizAccountId": "DUMMY_ACCOUNT_ID",
                                                "chargeId": charge_id,
                                                "status": "CARD_CHARGED",
                                                "orderId": isCorpOrder ? "" : order_id,
                                                "corpOrderId": isCorpOrder ? order_id : "",
                                                "isCorpOrder": isCorpOrder,
                                                "customerPaymentId": transaction_detail.customerPaymentId,
                                                "paymentCardId": charge.payment_method,
                                                "merchantAccountId": merchantAccountId,
                                                "additionalData": charge,
                                                "tax": 0,
                                                "totalAmt": charge.amount.toString().substring(0, charge.amount.toString().length - 2) + "." + charge.amount.toString().substring(charge.amount.toString().length - 2),
                                                "paidDate": new Date(charge.created * 1000).toISOString(),
                                                "forEstId": isCorpOrder ? transaction_detail.forEstId : ""
                                            }
                                        };
    
                                        request(options, function (error, response, body) {
                                            if (error) {
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                                            } else {
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                                // orderStatusUpdate(1, order_id, orderAmount, isCorpOrder, req, res);
                                                if (merchantAccountId === BEVVI_STRIPE_ACC) {
                                                    $tax_flag = true;
                                                } else {
                                                    $est_flag = true;
                                                }
                                                if ($est_flag && $tax_flag) {
                                                    $complete_flag = true;
                                                    orderStatusUpdate(1, order_id, originalOrderAmount, isCorpOrder, req, res);
                                                }
                                                
                                            }
                                        });
                                    } else {
                                        res.send({ "message": 'Error Capturing Charge, contact Bevvi', "status": "error" }).end();
                                    }
                                    
                                })
                                .catch((err) => {
                                    // $complete_flag = false;
                                    console.log('Error Capturing Charge');
                                    successlog.info(`capture_charge_b2b :: CAPTURE CHARGE :: error :: ${JSON.stringify(err)}`);
                                        if (err.code == "charge_expired_for_capture") {
                                            res.send({ "message": "Card Authorization is older than 7Days and is thus Expired", "status": "error" }).end();
                                        }
                                        else {
                                            res.send({ "message": err.code, "status": "error" }).end();
                                        }
                                });
                            } else if (action == "release" && transaction_detail.status === statusType) {
                                console.log(`For order ${index} `)
                                console.log('Release Capture');
                                // cancelPaymentIntents(charge_id, merchantAccountId)
                                await sendCancelPaymentIntent(type, {charge_id, merchantAccountId})
                                .then((captureCancelData) => {
                                    console.log('Success Captured');
                                    console.log("cancel_capture :: ",captureCancelData);
                                    // JSON.stringify(captureCancelData);
                                    
                                    if (captureCancelData.status === 'canceled') {
                                        var options = {
                                            method: 'POST',
                                            url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                            headers: { 'content-type': 'application/x-www-form-urlencoded'},
                                            form: {
                                                "accountId": transaction_detail.accountId,
                                                "bizAccountId": "DUMMY_ACCOUNT_ID",
                                                "chargeId": charge_id,
                                                "status": "CANCEL_CAPTURE",
                                                "orderId": isCorpOrder ? "" : order_id,
                                                "corpOrderId": isCorpOrder ? order_id : "",
                                                "isCorpOrder": isCorpOrder,
                                                "customerPaymentId": transaction_detail.customerPaymentId,
                                                "merchantAccountId": merchantAccountId,
                                                "additionalData": captureCancelData,
                                                "totalAmt": captureCancelData.amount.toString().substring(0, captureCancelData.amount.toString().length - 2) + "." + captureCancelData.amount.toString().substring(captureCancelData.amount.toString().length - 2),
                                                "paidDate": new Date(captureCancelData.created * 1000).toISOString(),
                                                "forEstId": isCorpOrder ? transaction_detail.forEstId : ""
                                            }
                                        };
        
                                        request(options, function(error, response, body) {
                                            if (error) {
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                                res.status(400).send(JSON.stringify(error)).end();
                                            } else {
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                                if (merchantAccountId === BEVVI_STRIPE_ACC) {
                                                    $tax_flag = true;
                                                } else {
                                                    $est_flag = true;
                                                }
                                                if ($est_flag && $tax_flag) {
                                                    $complete_flag = true;
                                                    orderStatusUpdate(3, order_id, originalOrderAmount, isCorpOrder, req, res);
                                                }
                                            }
                                        });
                                    } else {
                                        res.send({ "message": 'Error Capturing Charge, contact Bevvi', "status": "error" }).end();
                                    }
                                })
                                .catch((err) => {
                                    console.log('Error Cancelling Capture');
                                    console.log('Error Capturing',err);
                                    successlog.info(`cancel_charge :: CAPTURE CHARGE :: error :: ${JSON.stringify(err)}`);
                                    if (err.raw.code === "payment_intent_unexpected_state" || err.code == 'payment_intent_unexpected_state') {
                                        //res.send({ "message": "Card Authorization is older than 7Days and is thus Expired", "status": "error" }).end();
                                        $complete_flag = true;
                                        orderStatusUpdate(3, order_id, orderAmount, isCorpOrder, req, res);
                                    }
                                    else {
                                        res.send({ "message": "Server Error", "status": "error" }).end();
                                    }
                                });
                            } else if (action == "reject" && transaction_detail.status === statusType) {
                                console.log(`For order ${index} `)
                                console.log('To Refund Charge');
                                // refundPaymentIntents(charge_id, orderAmount, merchantAccountId)
                                await sendRefundPaymentIntent(type, {charge_id, orderAmount, merchantAccountId})
                                .then((refund) => {
                                    console.log("create_refund :: ", refund);
                                    JSON.stringify(refund);
                                    var options = {
                                        method: 'POST',
                                        url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                        form: {
                                            "accountId": transaction_detail.accountId,
                                            "bizAccountId": "DUMMY_ACCOUNT_ID",
                                            "chargeId": charge_id,
                                            "status": "CARD_REFUNDED",
                                            "orderId": isCorpOrder ? "" : order_id,
                                            "corpOrderId": isCorpOrder ? order_id : "",
                                            "isCorpOrder": isCorpOrder,
                                            "customerPaymentId": transaction_detail.customerPaymentId,
                                            "merchantAccountId": merchantAccountId,
                                            "additionalData": refund,
                                            "totalAmt": refund.amount.toString().substring(0, refund.amount.toString().length - 2) + "." + refund.amount.toString().substring(refund.amount.toString().length - 2),
                                            "paidDate": new Date(refund.created * 1000).toISOString(),
                                            "forEstId": isCorpOrder ? transaction_detail.forEstId : ""
                                        }
                                    };
    
                                    request(options, function (error, response, body) {
                                        if (error) {
                                            successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                                            successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                                            res.status(400).send(JSON.stringify(error)).end();
                                        } else {
                                            successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                            if (merchantAccountId === BEVVI_STRIPE_ACC) {
                                                $tax_flag = true;
                                            } else {
                                                $est_flag = true;
                                            }
                                            if ($est_flag && $tax_flag) {
                                                $complete_flag = true;
                                                orderStatusUpdate(5, order_id, originalOrderAmount, isCorpOrder, req, res);
                                            }
                                            // orderStatusUpdate(5, order_id, orderAmount, isCorpOrder, req, res);
                                        }
                                    });    
                                })
                                .catch((err) => {
                                    console.log('Error Refund Charge');
                                    console.log("create_refund :: error :: ", JSON.stringify(err));
                                    res.status(err.statusCode).send(JSON.stringify(err)).end();
                                });
                            } else if (action == "partial-refund" && transaction_detail.status === statusType) {
                                console.log('Partial Refund');
                                let refundAmt = 0;
                                if (transaction_detail.order.originalTotalAmount) {
                                    if (transaction_detail.order.originalTotalAmount === transaction_detail.order.totalAmount) {
                                        refundAmt = 0;
                                    } else {
                                        console.log('Original Amt',transaction_detail.order.originalTotalAmount);
                                        console.log('Final Amt',transaction_detail.order.totalAmount);
                                        refundAmt = parseFloat((transaction_detail.order.originalTotalAmount - transaction_detail.order.totalAmount).toFixed(2));
                                        console.log('Refund AMt', refundAmt)
                                    }
                                }
                                if (refundAmt > 0) {
                                    // refundPaymentIntents(charge_id, refundAmt, merchantAccountId)
                                    await sendRefundPaymentIntent(type, {charge_id, refundAmt, merchantAccountId})
                                    .then((refund) => {
                                        console.log('partial_refund ::',JSON.stringify(refund));
                                        var options = {
                                            method: 'POST',
                                            url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                            headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                            form: {
                                                "accountId": transaction_detail.accountId,
                                                "bizAccountId": "DUMMY_ACCOUNT_ID",
                                                "chargeId": charge_id,
                                                "status": "CARD_PARTIAL_REFUNDED",
                                                "orderId": isCorpOrder ? "" : order_id,
                                                "corpOrderId": isCorpOrder ? order_id : "",
                                                "isCorpOrder": isCorpOrder,
                                                "customerPaymentId": transaction_detail.customerPaymentId,
                                                "merchantAccountId": merchantAccountId,
                                                "additionalData": refund,
                                                "refundAmt": refundAmt,
                                                // "totalAmt": charge.amount.toString().substring(0, charge.amount.toString().length - 2) + "." + charge.amount.toString().substring(charge.amount.toString().length - 2),
                                                "paidDate": new Date(refund.created * 1000).toISOString(),
                                                "forEstId": isCorpOrder ? transaction_detail.forEstId : ""
                                            }
                                        }
    
                                        request(options, function(error, response,body) {
                                            if (error) {
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                                                res.status(400).send(JSON.stringify(error)).end();
                                            } else {
                                                successlog.info(`capture_charge_b2b :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                                if (merchantAccountId === BEVVI_STRIPE_ACC) {
                                                    $tax_flag = true;
                                                } else {
                                                    $est_flag = true;
                                                }
                                                if ($est_flag && $tax_flag) {
                                                    $complete_flag = true;
                                                    res.status(200).send({'status':'success', 'message':'Successfully provided partail refund', 'refundAmt': refundAmt});
                                                }
                                            }
                                        })
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                        res.status(err.statusCode).send(err);
                                    });
                                } else if (refundAmt === 0) {
                                    res.status(200).send({"status": 'success', 'messgae': 'refund not required', 'refundAmt': refundAmt})
                                }
                                
                            }
                        }
                        console.log('COMPLETED FLAG')
                        // if ($complete_flag) {
                        //     if (action == 'accept') {
                        //         console.log('Accept order')
                        //         orderStatusUpdate(1, order_id, originalOrderAmount, isCorpOrder, req, res);
                        //     } else if (action == 'release') {
                        //         console.log('release order')
                        //         orderStatusUpdate(3, order_id, originalOrderAmount, isCorpOrder, req, res);
                        //     } else if (action == 'reject') {
                        //         console.log('reject order')
                        //         orderStatusUpdate(5, order_id, originalOrderAmount, isCorpOrder, req, res);
                        //     } else if (action == 'partial-refund') {
                        //         res.status(200).send({'status':'success', 'message':'Successfully provided partail refund', 'refundAmt': refundAmt});
                        //     }
                        // }
                    }
                }
            }
        });
    }
}

router.post('/cancel_capture_charge_b2b', function(req, res, next) {
    if (req.body.paymentIntentId && req.body.connectedAccountId) {
        cancelPaymentIntents(req.body.paymentIntentId, req.body.connectedAccountId)
        .then((cancel) => {
            console.log(cancel);
            if (cancel.status === 'canceled') {
                console.log('cancel_capture_charge_b2b :: success ::', cancel.status);
                res.status(200).send({message: 'success'});
            } else {
                console.log('cancel_capture_charge_b2b :: success :: but status is ', cancel.status);
                res.status(400).send();
            }
        })
        .catch((error) => {
            console.log('Charge was not able to cancel', JSON.stringify(error));
            res.status(400).send();
        })
    }
})

/**
 * Other Functions
 */

router.post('/upload_product_image', upload.single('image'), function (req, res, next) {
    console.log(req.file);
    console.log(req.body);

    successlog.info(`upload_product_image :: REQUEST BODY :: ${JSON.stringify(req.file)}`);
    successlog.info(`upload_product_image :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'POST',
        url: API_ENDPOINT + 'productPhotos/upsertWithWhere?where={"productId":"' + req.body.productId + '"}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            "file": {
                "filename": req.file.key,
                "path": "storages/" + BUCKET_NAME + "/" + req.file.key
            },
            "productId": req.body.productId
        }
    };

    request(options, function (error, response, body) {
        if (JSON.parse(body).error) {
            res.send({ status: "error", message: "Please Try Again" }).end();
        } else {
            res.send({ status: "success", filename: req.file.key, path: "storages/" + BUCKET_NAME + "/" + req.file.key, message: "Image upload Successfully" }).end();
        }
    })
});

router.post('/upload_establishment_image', upload.single('image'), function (req, res, next) {
    console.log(req.file);
    console.log(req.body);

    successlog.info(`upload_establishment_image :: REQUEST BODY :: ${JSON.stringify(req.file)}`);
    successlog.info(`upload_establishment_image :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'POST',
        url: API_ENDPOINT + 'establishmentPhotos/upsertWithWhere?where={"establishmentId":"' + req.body.establishmentId + '"}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            "file": {
                "filename": req.file.key,
                "path": "storages/" + BUCKET_NAME + "/" + req.file.key
            },
            "establishmentId": req.body.establishmentId
        }
    };

    request(options, function (error, response, body) {
        if (JSON.parse(body).error) {
            res.send({ status: "error", message: "Please Try Again" }).end();
        } else {
            res.send({ status: "success", filename: req.file.key, path: "storages/" + BUCKET_NAME + "/" + req.file.key, message: "Image upload Successfully" }).end();
        }
    })
});

router.post('/upload_product_image_b2b', upload.single('image'), function (req, res, next) {
    console.log(req.file);
    console.log(req.body);

    successlog.info(`upload_product_image :: REQUEST BODY :: ${JSON.stringify(req.file)}`);
    successlog.info(`upload_product_image :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'POST',
        url: B2B_API_ENDPOINT + 'productPhotos/upsertWithWhere?where={"productId":"' + req.body.productId + '"}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            "file": {
                "filename": req.file.key,
                "path": "storages/" + BUCKET_NAME + "/" + req.file.key
            },
            "productId": req.body.productId
        }
    };

    request(options, function (error, response, body) {
        if (JSON.parse(body).error) {
            res.send({ status: "error", message: "Please Try Again" }).end();
        } else {
            res.send({ status: "success", filename: req.file.key, path: "storages/" + BUCKET_NAME + "/" + req.file.key, message: "Image upload Successfully" }).end();
        }
    })
});

router.post('/upload_establishment_image_b2b', upload.single('image'), function (req, res, next) {
    console.log(req.file);
    console.log(req.body);

    successlog.info(`upload_establishment_image :: REQUEST BODY :: ${JSON.stringify(req.file)}`);
    successlog.info(`upload_establishment_image :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'POST',
        url: B2B_API_ENDPOINT + 'establishmentPhotos/upsertWithWhere?where={"establishmentId":"' + req.body.establishmentId + '"}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            "file": {
                "filename": req.file.key,
                "path": "storages/" + BUCKET_NAME + "/" + req.file.key
            },
            "establishmentId": req.body.establishmentId
        }
    };

    request(options, function (error, response, body) {
        if (JSON.parse(body).error) {
            res.send({ status: "error", message: "Please Try Again" }).end();
        } else {
            res.send({ status: "success", filename: req.file.key, path: "storages/" + BUCKET_NAME + "/" + req.file.key, message: "Image upload Successfully" }).end();
        }
    })
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.send('This is the Bevvi Backend for Stripe. All services are running good. Enjoy.');
    res.end();
});

router.post('/ephemeral_keys', function (req, res, next) {
    console.log("ephemeral_keys req.body", req.body);
    const stripe_version = req.body.api_version;
    if (!stripe_version) {
        res.status(400).end();
        return;
    }
    stripe.ephemeralKeys.create(
        { customer: req.body.customerId },
        { apiVersion: stripe_version }
    ).then((key) => {
        console.log("ephemeral_keys :: ", key);
        res.status(200).json(key);
    }).catch((err) => {
        if (err) {
            console.log("ephemeral_keys :: error :: ", JSON.stringify(err));
            res.send(JSON.stringify(err)).status(500);
            res.end();
        }
    });
});

router.post('/ls_refreshToken', function (req, res, next) {
    console.log("req.body", req.body);
    console.log("req.query", req.query);

    var options = {
        method: 'POST',
        url: "https://cloud.lightspeedapp.com/oauth/access_token.php",
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            client_id: LIGHTSPEED_CLIENT_KEY,
            client_secret: LIGHTSPEED_CLIENT_SECRET,
            refresh_token: req.body.refresh_token,
            grant_type: "refresh_token"
        }
    };

    request(options, function (error, response, body) {
        successlog.info(`lightspeed ::  success :: ${JSON.stringify(body)}`);
        res.send(JSON.parse(body));
    });
});

router.get('/stripeOnboarding_b2b', function (req, res, next) {
    console.log('stripeOnboarding_b2b request', req);
    console.log('stripeOnboarding_b2b request query', req.query);
    var options = {
        method: 'POST',
        url: 'https://connect.stripe.com/oauth/token',
        headers: { 'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        formData:
        {
            client_secret: STRIPE_SK,
            code: req.query.code,
            grant_type: 'authorization_code'
        }
    };

    request(options, function (error, response, body) {
        console.log('stripeOnboarding_b2b checking token - response - ',response);
        console.log('stripeOnboarding_b2b checking token body -', body);
        stripe.accounts.retrieve(
            JSON.parse(body).stripe_user_id,
            function (err, account) {
              console.log('stripeOnboarding_b2b Account - ', account);
                // var options = {
                //     method: 'GET'
                // }
                var options = {
                    method: 'GET',
                    url: B2B_API_ENDPOINT + 'bevviutils/getAccountId?acctType=3&emailId=' + account.email,
                };

                request(options, function (error, response, body) {
                    if (JSON.parse(body).message) {
                        fs.readFile("views/stripeOnboardingError_b2b.html", function (error, stripeOnboarding) {
                            if (error) {
                                res.writeHead(404);
                                res.write('Contents you are looking are Not Found');
                            } else {
                                res.writeHead(200, { 'Content-Type': 'text/html' });
                                res.write(stripeOnboarding);
                            }
                            res.end();
                        });
                    } else {
                        let formData = {
                            bizAccountId: JSON.parse(body).result.id,
                            token: account.id,
                            type: "stripe",
                            bizaccountId: JSON.parse(body).result.id,
                            email: account.email,
                            businessName: account.business_profile.name ? account.business_profile.name : ""
                        };
                        console.log("FormData", formData)

                        var options = {
                            method: 'POST',
                            url: B2B_API_ENDPOINT + 'paymentIds',
                            headers: { 'content-type': 'application/x-www-form-urlencoded' },
                            form: formData
                        };

                        request(options, function (error, response, body) {
                            fs.readFile("views/stripeOnboarding_b2b.html", function (error, stripeOnboarding) {
                                if (error) {
                                    res.writeHead(404);
                                    res.write('Contents you are looking are Not Found');
                                } else {
                                    res.writeHead(200, { 'Content-Type': 'text/html' });
                                    res.write(stripeOnboarding);
                                }
                                res.end();
                            });
                        });
                    }
                });
            }
        );
    });
});

router.get('/getTaxRate/:latitude/:longitude', function (req, res, next) {
    /*
        //DEVELOPMENT
        res.send({
            zip: '10011',
            country: 'US',
            country_rate: '0.0',
            state: 'NY',
            state_rate: '0.04',
            county: 'NEW YORK',
            county_rate: '0.0',
            city: 'NEW YORK CITY',
            city_rate: '0.04875',
            combined_district_rate: '0.0',
            combined_rate: '0.08875',
            freight_taxable: true
        });
        res.end();
    */

// var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + req.params.latitude + "," + req.params.longitude + "&result_type=postal_code&key=" + GOOGLE_API_KEY;
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + req.params.latitude + "," + req.params.longitude + "&key=" + GOOGLE_API_KEY;
    request(url, (error, response, body) => {
        // console.log(body);
        if (JSON.parse(body).status == "OK") {
            let searchAddressComponents = JSON.parse(body).results[0].address_components;
            let searchPostalCode = "";
            let locality = '';
            searchAddressComponents.forEach(component => {
                if (component.types[0] == "postal_code") {
                    searchPostalCode = component.short_name;
                }
                if (component.types[0] == "locality" || component.types[0] == "political") {
                    locality = component.short_name;
                }
            });
            // console.log('getTaxRate success', searchPostalCode);
            // successlog.info(`getTaxRate ::  success :: ${searchPostalCode}`);

            if (searchPostalCode == "") {
                console.log('getTaxRate error', "NO ZIP CODE FOUND FOR THE GIVEN LATLONG");
                successlog.info(`getTaxRate ::  error :: NO ZIP CODE FOUND FOR THE GIVEN LATLONG ${req.params.latitude} ${req.params.longitude}`);
                res.send({
                    zip: '10011',
                    country: 'US',
                    country_rate: '0.0',
                    state: 'NY',
                    state_rate: '0.04',
                    county: 'NEW YORK',
                    county_rate: '0.0',
                    city: 'NEW YORK CITY',
                    city_rate: '0.04875',
                    combined_district_rate: '0.0',
                    combined_rate: '0.08875',
                    freight_taxable: true
                });
            } else {
                console.log('getTaxRate success', searchPostalCode);
                successlog.info(`getTaxRate ::  success :: ${searchPostalCode}`);
                taxjar.ratesForLocation(searchPostalCode)
                    .then(function (response) {
                        // console.log(response);
                        response.rate;
                        if (locality === 'Washington') {
                            response.rate.combined_rate = '0.1025'
                        }
                        console.log(response.rate);
                        res.status(200).send(response.rate).end();
                    })
                    .catch(function (error) {
                        console.log(error);
                        res.status(200).send({
                            zip: '10011',
                            country: 'US',
                            country_rate: '0.0',
                            state: 'NY',
                            state_rate: '0.04',
                            county: 'NEW YORK',
                            county_rate: '0.0',
                            city: 'NEW YORK CITY',
                            city_rate: '0.04875',
                            combined_district_rate: '0.0',
                            combined_rate: '0.08875',
                            freight_taxable: true
                        }).end();
                    });
            }
        } else {
            console.log('getTaxRate error', JSON.parse(body).status);
            successlog.info(`getTaxRate ::  error :: ${JSON.parse(body).status}`);
            res.send({
                zip: '10011',
                country: 'US',
                country_rate: '0.0',
                state: 'NY',
                state_rate: '0.04',
                county: 'NEW YORK',
                county_rate: '0.0',
                city: 'NEW YORK CITY',
                city_rate: '0.04875',
                combined_district_rate: '0.0',
                combined_rate: '0.08875',
                freight_taxable: true
            }).end();
        }
    });
});

/* Old router function */

router.post('/create_charge', function (req, res, next) {
    var shoppingCartBody = {};
    var chargeId = "";
    var transactionId = "";
    var orderId = "";
    var tmpTotalPrice = 0;
    console.log("create_charge req.body", req.body);
    successlog.info(`create_charge :: REQUEST BODY :: ${JSON.stringify(req.body)}`);

    var options = {
        method: 'GET',
        url: API_ENDPOINT + 'accounts?access_token=' + req.body.accessToken + '&filter={"where":{"id":"' + req.body.accountId + '"}}',
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
    };

    request(options, function (error, response, body) {
        if (error) {

        } else {
            if (JSON.parse(body).error) {
                res.send({ "message": JSON.parse(body).error.message, "status": "error" }).end();
            } else {
                //FETCH SHOPPING CART
                var options = {
                    method: 'GET',
                    url: API_ENDPOINT + 'shoppingcarts?filter={"where":{"accountId":"' + req.body.accountId + '","state":0},"include":["offer"]}',
                    headers: { 'content-type': 'application/x-www-form-urlencoded' }
                };

                request(options, function (error, response, body) {
                    if (error) {
                        successlog.info(`create_charge :: USERS SHOPPING CART :: error :: ${JSON.stringify(error)}`);
                        successlog.info(`create_charge :: USERS SHOPPING CART :: error :: ${JSON.stringify(body)}`);
                    } else {
                        successlog.info(`create_charge :: USERS SHOPPING CART :: ${JSON.stringify(body)}`);
                        console.log(JSON.stringify(body));
                        shoppingCartBody = JSON.parse(body);

                        shoppingCartBody.forEach(function (item) {
                            tmpTotalPrice += item.offer.salePrice * item.quantity;
                        });

                        let finalTotalPrice = parseInt((((tmpTotalPrice + parseFloat(req.body.taxAmount)) - req.body.discountApplied.discountAmount) * 100).toFixed(2));
                        successlog.info(`++++++++++++++++++++++++++++++++ Amount from web ${req.body.amount}`);
                        successlog.info(`++++++++++++++++++++++++++++++++ finalTotalPrice ${finalTotalPrice}`);

                        if (tmpTotalPrice < 1) {
                            res.send({ "discountUsed": "Total Amount is less than $1.00. Order cannot be placed", "status": "error" }).end();
                        } else {
                            stripe.charges.create({
                                amount: req.body.amount,
                                currency: "usd",
                                customer: req.body.customerId,
                                source: req.body.source,
                                // stripe_account: req.body.destination_account,
                                // destination: {
                                //     account: req.body.destination_account,
                                // },
                                capture: false
                            }, {stripe_account: req.body.destination_account}, function (err, charge) {
                                if (err) {
                                    successlog.info(`create_charge :: CHARGE CREATED ON USER :: error :: ${JSON.stringify(err)}`);
                                    console.log("create_charge :: CHARGE CREATED ON USER :: error :: ", JSON.stringify(err));
                                    res.send(JSON.stringify(err));
                                    res.end();
                                } else {
                                    successlog.info(`create_charge :: CHARGE CREATED ON USER :: ${JSON.stringify(charge)}`);
                                    console.log("create_charge :: CHARGE CREATED ON USER :: ", charge);
                                    chargeId = charge.id;

                                    //CREATE TRANSACTION
                                    var options = {
                                        method: 'POST',
                                        url: API_ENDPOINT + 'transactions?access_token=' + req.body.accessToken,
                                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                        form: {
                                            "accountId": req.body.accountId,
                                            "bizAccountId": "DUMMY_ACCOUNT_ID",
                                            "chargeId": chargeId,
                                            "status": "CARD_AUTHORIZED",
                                            "orderId": "",
                                            "customerPaymentId": req.body.customerId,
                                            "paymentCardId": req.body.source,
                                            "merchantAccountId": req.body.destination_account,
                                            "additionalData": charge,
                                            "tax": req.body.taxAmount,
                                            "totalAmt": req.body.amount.toString().substring(0, req.body.amount.toString().length - 2) + "." + req.body.amount.toString().substring(req.body.amount.toString().length - 2),
                                            "paidDate": new Date(charge.created * 1000).toISOString()
                                        }
                                    };

                                    request(options, function (error, response, body) {
                                        if (error) {
                                            successlog.info(`create_charge :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                                            successlog.info(`create_charge :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                                        } else {
                                            successlog.info(`create_charge :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                            console.log(JSON.stringify(body));
                                            transactionId = JSON.parse(body).id;


                                            tmpQuantity = 0;
                                            shoppingCartBody.forEach(function (item) {
                                                tmpQuantity += item.quantity
                                            });

                                            discountInfo = {};
                                            if (typeof req.body.discountApplied == "string") {
                                                discountInfo = JSON.parse(req.body.discountApplied);
                                            } else if (typeof req.body.discountApplied == "object") {
                                                discountInfo = req.body.discountApplied
                                            }

                                            //CREATE ORDER
                                            var options = {
                                                method: 'POST',
                                                url: API_ENDPOINT + 'orders',
                                                headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                                form: {
                                                    "accountId": req.body.accountId,
                                                    "establishmentId": req.body.establishmentId,
                                                    "pickupTime": req.body.pickupDateTime,
                                                    "totalAmount": req.body.amount.substr(0, req.body.amount.length - 2) + "." + req.body.amount.substr(-2),
                                                    "tax": parseFloat(req.body.taxAmount).toFixed(2),
                                                    "qty": parseInt(tmpQuantity),
                                                    "taxPercent": (req.body.taxPercent) ? parseFloat(req.body.taxPercent) : 12.99,
                                                    "discountApplied": discountInfo
                                                }
                                            };

                                            request(options, function (error, response, body) {
                                                if (error) {
                                                    successlog.info(`create_charge :: ORDER CREATED :: error :: ${JSON.stringify(error)}`);
                                                    successlog.info(`create_charge :: ORDER CREATED :: error :: ${JSON.stringify(body)}`);
                                                    return true;
                                                } else {
                                                    orderId = JSON.parse(body).id;

                                                    var options = {
                                                        method: 'GET',
                                                        url: API_ENDPOINT + 'orders/createNewOrderNotification?orderId=' + orderId,
                                                        headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                    };

                                                    request(options);

                                                    successlog.info(`create_charge :: ORDER CREATED :: ${JSON.stringify(body)}`);

                                                    //UPDATE TRANSACTION WITH ORDER ID
                                                    var options = {
                                                        method: 'PATCH',
                                                        url: API_ENDPOINT + 'transactions/' + transactionId + '?access_token=' + req.body.accessToken,
                                                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                                        form: {
                                                            "orderId": orderId
                                                        }
                                                    };

                                                    request(options, function (error, response, body) {
                                                        if (error) {
                                                            successlog.info(`create_charge :: UPDATE TRANSACTION WITH ORDER ID :: error :: ${JSON.stringify(error)}`);
                                                            successlog.info(`create_charge :: UPDATE TRANSACTION WITH ORDER ID :: error :: ${JSON.stringify(body)}`);
                                                            return true;
                                                        } else {
                                                            successlog.info(`create_charge :: UPDATE TRANSACTION WITH ORDER ID :: ${JSON.stringify(body)}`);
                                                            shoppingCartBody.forEach((element, index) => {
                                                                var options = {
                                                                    method: 'POST',
                                                                    url: API_ENDPOINT + 'orderdetails',
                                                                    headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                                                    form: {
                                                                        "productId": element.productId,
                                                                        "orderId": orderId,
                                                                        "price": element.offer.salePrice,
                                                                        "actualPrice": element.offer.originalPrice,
                                                                        "quantity": element.quantity,
                                                                        "offerId": element.offerId
                                                                    }
                                                                };

                                                                request(options, function (error, response, body) {
                                                                    if (error) {
                                                                        successlog.info(`create_charge :: CREATE ORDERDETAIL :: error :: ${JSON.stringify(error)}`);
                                                                        successlog.info(`create_charge :: CREATE ORDERDETAIL :: error :: ${JSON.stringify(body)}`);
                                                                    } else {
                                                                        successlog.info(`create_charge :: CREATE ORDERDETAIL :: ${JSON.stringify(body)}`);
                                                                        if (index == (shoppingCartBody.length - 1)) {
                                                                            //GIVE RESPONSE TO MOBILE DEVICE
                                                                            var options = {
                                                                                method: 'GET',
                                                                                url: API_ENDPOINT + 'orders?filter={"where":{"id":"' + orderId + '","status":0},"include":["transactions","establishment",{"orderdetails":["product"]}]}',
                                                                                headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                                            };

                                                                            request(options, function (error, response, body) {
                                                                                if (error) {
                                                                                    successlog.info(`create_charge :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(error)}`);
                                                                                    successlog.info(`create_charge :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(body)}`);
                                                                                    res.send(JSON.stringify(err));
                                                                                    res.end();
                                                                                } else {
                                                                                    successlog.info(`create_charge :: RESPONSE TO MOBILE :: ${JSON.stringify(body)}`);
                                                                                    res.send(JSON.parse(body));
                                                                                    res.end();
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }
    });
});

router.post('/capture_charge', function (req, res, next) {
    console.log("capture_charge req.body", req.body);
    successlog.info(`capture_charge :: REQUEST BODY :: ${JSON.stringify(req.body)}`);
    var order_id = req.body.order_id;
    var access_token = req.body.access_token;
    var action = req.body.action;
    var charge_id = "";
    var transaction_detail = "";
    if (typeof order_id == "undefined" || order_id == "") {
        res.send({ "message": "Order Id is required", "status": "error" });
        res.end();
    } else if (typeof action == "undefined" || action == "") {
        res.send({ "message": "Action is required", "status": "error" });
        res.end();
    } else if (typeof access_token == "undefined" || access_token == "") {
        res.send({ "message": "Access Token is required", "status": "error" });
        res.end();
    } else {
        //FETCH TRANSACTION
        var options = {
            method: 'GET',
            url: API_ENDPOINT + 'transactions?filter={"where":{"orderId":"' + order_id + '"}}&access_token=' + access_token,
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        };

        request(options, function (error, response, body) {
            if (error) {
                successlog.info(`capture_charge :: TRANSACTION FOR CURRENT ORDER :: error :: ${JSON.stringify(error)}`);
                successlog.info(`capture_charge :: TRANSACTION FOR CURRENT ORDER :: error :: ${JSON.stringify(body)}`);
                res.send({ "message": "Error Occured", "status": "error" });
                res.end();
            } else {
                successlog.info(`capture_charge :: TRANSACTION FOR CURRENT ORDER :: ${JSON.stringify(body)}`);
                if (error) {

                } else {
                    if (JSON.parse(body).error) {
                        res.send({ "message": JSON.parse(body).error.message, "status": "error", "status_code": "SESSION_EXPIRED" }).end();
                    } else {
                        transaction_detail = JSON.parse(body)[0];
                        charge_id = transaction_detail.chargeId;

                        if (action == "accept") {
                            stripe.charges.capture(charge_id, { expand: ['balance_transaction'] }, function (err, charge) {
                                if (err) {
                                    successlog.info(`capture_charge :: CAPTURE CHARGE :: error :: ${JSON.stringify(err)}`);
                                    if (err.code == "charge_expired_for_capture") {
                                        res.send({ "message": "Card Authorization is older than 7Days and is thus Expired", "status": "error" }).end();
                                    } else {
                                        res.send({ "message": err.code, "status": "error" }).end();
                                    }
                                } else {
                                    successlog.info(`capture_charge :: CAPTURE CHARGE :: ${JSON.stringify(charge)}`);

                                    //CREATE TRANSACTION
                                    var options = {
                                        method: 'POST',
                                        url: API_ENDPOINT + 'transactions?access_token=' + access_token,
                                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                        form: {
                                            "accountId": transaction_detail.accountId,
                                            "bizAccountId": "DUMMY_ACCOUNT_ID",
                                            "chargeId": charge_id,
                                            "status": "CARD_CHARGED",
                                            "orderId": order_id,
                                            "customerPaymentId": transaction_detail.customerPaymentId,
                                            "paymentCardId": charge.source.id,
                                            "merchantAccountId": charge.destination,
                                            "additionalData": charge,
                                            "tax": 0,
                                            "totalAmt": charge.amount.toString().substring(0, charge.amount.toString().length - 2) + "." + charge.amount.toString().substring(charge.amount.toString().length - 2),
                                            "paidDate": new Date(charge.created * 1000).toISOString()
                                        }
                                    };

                                    request(options, function (error, response, body) {
                                        if (error) {
                                            successlog.info(`capture_charge :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                                            successlog.info(`capture_charge :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                                        } else {
                                            successlog.info(`capture_charge :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);

                                            //UPDATE ORDER WITH STATUS 1
                                            var options = {
                                                method: 'PATCH',
                                                url: API_ENDPOINT + 'orders/' + order_id + '?access_token=' + req.body.accessToken,
                                                headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                                form: {
                                                    "status": 1
                                                }
                                            };

                                            request(options, function (error, response, body) {
                                                if (error) {
                                                    successlog.info(`capture_charge :: UPDATE ORDER TO PENDING PICKUP :: error :: ${JSON.stringify(error)}`);
                                                    successlog.info(`capture_charge :: UPDATE ORDER TO PENDING PICKUP :: error :: ${JSON.stringify(body)}`);
                                                    return true;
                                                } else {
                                                    successlog.info(`capture_charge :: UPDATE ORDER TO PENDING PICKUP :: ${JSON.stringify(body)}`);

                                                    //GIVE RESPONSE TO MOBILE DEVICE
                                                    var options = {
                                                        method: 'GET',
                                                        url: API_ENDPOINT + 'orders?filter={"where":{"id":"' + order_id + '"}}',
                                                        headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                    };

                                                    request(options, function (error, response, body) {
                                                        if (error) {
                                                            successlog.info(`capture_charge :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(error)}`);
                                                            successlog.info(`capture_charge :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(body)}`);
                                                            res.send(JSON.stringify(err));
                                                            res.end();
                                                        } else {
                                                            successlog.info(`capture_charge :: RESPONSE TO MOBILE :: ${JSON.stringify(body)}`);
                                                            res.send(JSON.parse(body));
                                                            res.end();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        } else if (action == "reject") {
                            stripe.refunds.create({
                                charge: charge_id
                            }, function (err, refund) {
                                if (err) {
                                    console.log("create_refund :: error :: ", JSON.stringify(err));
                                    res.send(JSON.stringify(err));
                                    res.end();
                                } else {
                                    console.log("create_refund :: ", refund);
                                    JSON.stringify(refund);
                                    var options = {
                                        method: 'POST',
                                        url: API_ENDPOINT + 'transactions?access_token=' + access_token,
                                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                        form: {
                                            "accountId": transaction_detail.accountId,
                                            "bizAccountId": "DUMMY_ACCOUNT_ID",
                                            "chargeId": charge_id,
                                            "status": "CARD_REFUNDED",
                                            "orderId": order_id,
                                            "customerPaymentId": transaction_detail.customerPaymentId,
                                            "additionalData": refund,
                                            "paidDate": new Date(refund.created * 1000).toISOString()
                                        }
                                    };

                                    request(options, function (error, response, body) {
                                        if (error) {
                                            successlog.info(`capture_charge :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                                            successlog.info(`capture_charge :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                                        } else {
                                            successlog.info(`capture_charge :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);

                                            //UPDATE ORDER WITH STATUS 3 REJECTED
                                            var options = {
                                                method: 'PATCH',
                                                url: API_ENDPOINT + 'orders/' + order_id + '?access_token=' + req.body.accessToken,
                                                headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                                form: {
                                                    "status": 3
                                                }
                                            };

                                            request(options, function (error, response, body) {
                                                if (error) {
                                                    successlog.info(`capture_charge :: UPDATE ORDER TO REJECTED :: error :: ${JSON.stringify(error)}`);
                                                    successlog.info(`capture_charge :: UPDATE ORDER TO REJECTED :: error :: ${JSON.stringify(body)}`);
                                                    return true;
                                                } else {
                                                    successlog.info(`capture_charge :: UPDATE ORDER TO REJECTED :: ${JSON.stringify(body)}`);

                                                    //GIVE RESPONSE TO MOBILE DEVICE
                                                    var options = {
                                                        method: 'GET',
                                                        url: API_ENDPOINT + 'orders?filter={"where":{"id":"' + order_id + '"}}',
                                                        headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                    };

                                                    request(options, function (error, response, body) {
                                                        if (error) {
                                                            successlog.info(`capture_charge :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(error)}`);
                                                            successlog.info(`capture_charge :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(body)}`);
                                                            res.send(JSON.stringify(err));
                                                            res.end();
                                                        } else {
                                                            successlog.info(`capture_charge :: RESPONSE TO MOBILE :: ${JSON.stringify(body)}`);
                                                            res.send(JSON.parse(body));
                                                            res.end();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }
        });
    }
});

router.post('/create_refund', function (req, res, next) {
    console.log("create_refund req.body", req.body);
    stripe.refunds.create({
        charge: req.body.charge_id
    }, function (err, refund) {
        if (err) {
            console.log("create_refund :: error :: ", JSON.stringify(err));
            res.send(JSON.stringify(err));
            res.end();
        } else {
            console.log("create_refund :: ", refund);
            res.send(JSON.stringify(refund));
            res.end();
        }
    });
});

router.post('/create-customer', function (req, res, next) {
    successlog.info(`create-customer :: ${JSON.stringify(req.body)}`);
    console.log("create-customer req.body", req.body);

    stripe.customers.create({
        email: req.body.email,
    }, function (err, customer) {
        if (err) {
            console.log("create-customer :: error :: ", JSON.stringify(err));
            successlog.info(`create-customer :: error :: ${JSON.stringify(err)}`);
            res.send(JSON.stringify(err));
            res.end();
        } else {
            console.log("create-customer :: ", customer);
            successlog.info(`create-customer :: ${JSON.stringify(customer)}`);
            var options = {
                method: 'POST',
                url: API_ENDPOINT + 'paymentIds',
                headers: { 'content-type': 'application/x-www-form-urlencoded' },
                form: {
                    accountId: req.body.accountId,
                    token: customer.id,
                    type: 'stripe'
                }
            };

            request(options, function (error, response, body) {
                if (error) {
                    successlog.info(`create-customer :: ${JSON.stringify(error)}`);
                    successlog.info(`create-customer :: ${JSON.stringify(body)}`);
                    res.end();
                    // return true;
                } else {
                    successlog.info(`create-customer :: ${JSON.stringify(body)}`);
                    res.end();
                    // return true;
                }
            });
        }
    });
});

router.post('/create_token', function (req, res, next) {
    console.log("create_token req.body", req.body);
    stripe.tokens.create({
        card: {
            "number": req.body.number,
            "exp_month": req.body.exp_month,
            "exp_year": req.body.exp_year,
            "cvc": req.body.cvv
        }
    }, function (err, token) {
        if (err) {
            console.log("create_token :: ", err);
            res.send(JSON.stringify(err));
            res.end();
        } else {
            console.log("create_token :: ", token);
            stripe.sources.create({
                token: token.id,
                customer: 'cus_CCaDqZ88n5ZAbR',

            }, function (err, source) {
                console.log("err ::", err);// asynchronously called
                console.log("source :: ", source)
            });
            res.send(JSON.stringify(token));
            res.end();
        }
    });
});

router.get('/ls_authorize', function (req, res, next) {
    console.log("req.body", req.body);
    console.log("req.query", req.query);

    var permanentAccessToken = "";
    var accessTokenResponse;

    var options = {
        method: 'POST',
        url: "https://cloud.lightspeedapp.com/oauth/access_token.php",
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            client_id: LIGHTSPEED_CLIENT_KEY,
            client_secret: LIGHTSPEED_CLIENT_SECRET,
            code: req.query.code,
            grant_type: "authorization_code"
        }
    };

    request(options, function (error, response, body) {
        successlog.info(`lightspeed ::  success :: ${JSON.stringify(body)}`);
        console.log(body);
        if (response.statusCode == 400) {
            var renderParams = {
                message: JSON.parse(body).error_description
            };
            res.render('index', renderParams);
        } else {
            accessTokenResponse = JSON.parse(body);
            permanentAccessToken = JSON.parse(body).access_token;

            var options = {
                method: 'GET',
                url: "https://api.lightspeedapp.com/API/Account.json",
                headers: {
                    'content-type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + permanentAccessToken
                }
            };

            request(options, function (error, response, body) {
                successlog.info(`lightspeed ::  success :: ${JSON.stringify(body)}`);
                if (response.statusCode == 400) {
                    var renderParams = {
                        message: JSON.parse(body).error_description
                    };
                    res.render('index', renderParams);
                } else {
                    accessTokenResponse.accountID = JSON.parse(body).Account.accountID;
                    accessTokenResponse.name = JSON.parse(body).Account.name;
                    accessTokenResponse.state = req.query.state;

                    //CREATE POS INFO Entry
                    var options = {
                        method: 'POST',
                        url: B2B_API_ENDPOINT + 'posinfo',
                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                        form: {
                            "establishmentId": accessTokenResponse.state,
                            "name": accessTokenResponse.name,
                            "description": "",
                            "accessToken": accessTokenResponse.access_token,
                            "tokenType": accessTokenResponse.token_type,
                            "scope": accessTokenResponse.scope,
                            "accountId": accessTokenResponse.accountID,
                            "refreshToken": accessTokenResponse.refresh_token,
                            "pos": "LS"
                        }
                    };

                    request(options, function (error, response, body) {
                        var stream = fs.createWriteStream("./logs/lightspeed.log", { flags: 'a' });
                        stream.write(JSON.stringify(accessTokenResponse) + "\n\n");
                        stream.end();

                        var renderParams = {
                            message: "Thank You for connecting to Bevvi"
                        };
                        res.render('index', renderParams);
                    });
                }
            });
        }
    });
});

router.get('/stripeOnboarding', function (req, res, next) {
    console.log(req.query.code);
    var options = {
        method: 'POST',
        url: 'https://connect.stripe.com/oauth/token',
        headers: { 'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        formData:
        {
            client_secret: STRIPE_SK,
            code: req.query.code,
            grant_type: 'authorization_code'
        }
    };

    request(options, function (error, response, body) {
        console.log(body);
        stripe.accounts.retrieve(
            JSON.parse(body).stripe_user_id,
            function (err, account) {
                console.log(JSON.parse(body).stripe_user_id)
                var options = {
                    method: 'GET',
                    url: API_ENDPOINT + 'bevviutils/getAccountId?acctType=1&emailId=' + account.email,
                };

                request(options, function (error, response, body) {
                    if (JSON.parse(body).message) {
                        fs.readFile("views/stripeOnboardingError.html", function (error, stripeOnboarding) {
                            if (error) {
                                res.writeHead(404);
                                res.write('Contents you are looking are Not Found');
                            } else {
                                res.writeHead(200, { 'Content-Type': 'text/html' });
                                res.write(stripeOnboarding);
                            }
                            res.end();
                        });
                    } else {
                        let formData = {
                            bizAccountId: JSON.parse(body).result.id,
                            token: account.id,
                            type: "stripe",
                            bizaccountId: JSON.parse(body).result.id
                        };
                        console.log("FormData", formData)

                        var options = {
                            method: 'POST',
                            url: API_ENDPOINT + 'paymentIds',
                            headers: { 'content-type': 'application/x-www-form-urlencoded' },
                            form: formData
                        };

                        request(options, function (error, response, body) {
                            fs.readFile("views/stripeOnboarding.html", function (error, stripeOnboarding) {
                                if (error) {
                                    res.writeHead(404);
                                    res.write('Contents you are looking are Not Found');
                                } else {
                                    res.writeHead(200, { 'Content-Type': 'text/html' });
                                    res.write(stripeOnboarding);
                                }
                                res.end();
                            });
                        });
                    }
                });
            }
        );
    });
});

/* End of Old Router function */
router.get('/faq', function (req, res, next) {
    fs.readFile("views/faq.html", function (error, faqContent) {
        if (error) {
            res.writeHead(404);
            res.write('Contents you are looking are Not Found');
        } else {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(faqContent);
        }

        res.end();
    });
});

router.get('/privacyPolicy', function (req, res, next) {
    fs.readFile("views/privacyPolicy.html", function (error, faqContent) {
        if (error) {
            res.writeHead(404);
            res.write('We are working on our Privacy Policy. Will push it up soon.');
        } else {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(faqContent);
        }

        res.end();
    });
});

router.get('/termsOfService', function (req, res, next) {
    fs.readFile("views/termsOfService.html", function (error, faqContent) {
        if (error) {
            res.writeHead(404);
            res.write('We are working on our Terms of Service. Will push it up soon.');
        } else {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(faqContent);
        }

        res.end();
    });
});

router.get('/testFunc', function (req, res, next) {
    stripe.customers.create({
        email: 'foo-customer@example.com'
    }).then(function (customer) {
        return stripe.customers.createSource(customer.id, {
            source: {
                object: 'source',
                type: "card",
                card: {
                    exp_year: 2020,
                    number: 4242424242424242,
                    cvc: "892",
                    exp_month: 2
                }
            }
        });
    })
});

router.post('/confirm_payment', function (req, res, next) {
    let connectedAccountId = req.body.connectedAccountId;
    let paymentIntentId = req.body.paymentIntentId;
    let paymentMethodId = req.body.paymentMethodId;
    confirmPaymentIntents(paymentIntentId, paymentMethodId, connectedAccountId)
    .then((charge) => {
        res.status(200).send(charge);
    })
    .catch((err) => {
        res.status(402).send(err);
    })
});

// http://localhost:4200/cart-placeorder?payment_intent=pi_1HzjWUGWFt45YVyJWgqbhsB0&payment_intent_client_secret=pi_1HzjWUGWFt45YVyJWgqbhsB0_secret_H7w0VpiQnFuo7Gj7hWhUa4dDX

router.post('stripe_callback', function(req, res,next) {
    
})

// Taxjar
router.post('/getTaxRateOnZipcode', function(req, res, next) {
    let zipcode = req.body.zipcode;
    if (zipcode) {
        taxjar.ratesForLocation(zipcode)
        .then(function (response) {
            response.rate;
            console.log(response.rate);
            res.status(200).send(response.rate).end();
        })
        .catch(function (error) {
            console.log(error);
            res.status(400).send({
                code: 'error'
            }).end();
        })
    }
})

/**
 * Async function for Accounts
 */

async function listAllAccount() {
    return await stripe.accounts.list();
}

/**
 * Asycn Function for Customer
 */


/**
 * Create Customer
 * @param {string} customerId 
 * @return {promise} create
 */
async function createCustomer(email) {
    console.log('Create New Customer for', email);
    return await stripe.customers.create({email: email});
}

/**
 * Update Customer with Source Info
 * @param {object} updateData 
 * @return {promise} update
 */
async function updateCustomerSourceInfo(updateData) {
    console.log(updateData);
    if (updateData.type === 'source') {
        return await stripe.customers.update(updateData.customerId, {
            default_source: updateData.sourceId,
            invoice_settings: {
                default_payment_method: ''
            }
        });
    } else {
        return await stripe.customers.update(updateData.customerId, {
            invoice_settings: {
                default_payment_method: updateData.sourceId
            }
        });
    }
}

/**
 * Retrieve Customer 
 * @param {string} customerId 
 * @return {promise} retrieveCustomer
 */
async function retrieveCustomer(customerId) {
    console.log('CustomerID: ',typeof customerId)
    return await stripe.customers.retrieve(customerId);
}

/**
 * Async Function for Cards
 */

/**
 * Create New Card
 * @param {object} sourceInfo 
 * @return {promise} createSource
 */
async function createCard(sourceInfo) {
    console.log(sourceInfo);
    return await stripe.customers.createSource(sourceInfo.customerId, {source: sourceInfo.source});
}

/**
 * Retrieve Card
 * @param {object} sourceInfo 
 * @return {promise} retrieveCard
 */
async function retrieveCard(sourceInfo) {
    console.log(sourceInfo);
    return await stripe.customers.retrieveSource(sourceInfo.customerId, sourceInfo.sourceId);
}

/**
 * Update Card
 * @param {object} sourceInfo 
 * @return {promise} deleteSource
 */
async function updateCard(sourceInfo) {
    console.log(sourceInfo);
    let source = {};
    Object.keys(sourceInfo.source).map((key, info) => {
        if (sourceInfo.source[key]) {
            source[key] = sourceInfo.source[key];
        }
    });
    console.log(source);
    return await stripe.customers.updateSource(sourceInfo.customerId, sourceInfo.sourceId, source);
}

/**
 * Delete Card
 * @param {object} sourceInfo 
 * @return {promise} deleteSource
 */
async function deleteCard(sourceInfo) {
    console.log(sourceInfo);
    return await stripe.customers.deleteSource(sourceInfo.customerId, sourceInfo.sourceId);
}

/**
 * List All Cards of Customer
 * @param {string} customerId 
 * @param {string} limit 
 * @return {promise} listSources
 */
async function listAllSourceCard(customerId, limit) {
    console.log('limit', limit);
    return await stripe.customers.listSources(customerId, {
        limit: limit
    });
}

/**
 * Async Function for Payment Methods
 */

/**
 * Create Payment Methods
 * @param {object} sourceInfo 
 * @return {promise} create
 */
async function createPaymentMethod(sourceInfo) {
    console.log('Create Payment Method');
    return await stripe.paymentMethods.create({
        type:'card',
        card: {
            number: sourceInfo.source.card.number,
            exp_month: sourceInfo.source.card.exp_month,
            exp_year: sourceInfo.source.card.exp_year,
            cvc: sourceInfo.source.card.cvc
        }
    });
}

/**
 * Create Payment Methods For Corporate
 * @param {object} sourceInfo 
 * @return {promise} create
 */
 async function createPaymentMethodCorporate(sourceInfo) {
    console.log('Create Payment Method');
    return await stripe.paymentMethods.create({
        type:'card',
        card: {
            number: sourceInfo.source.card.number,
            exp_month: sourceInfo.source.card.exp_month,
            exp_year: sourceInfo.source.card.exp_year,
            cvc: sourceInfo.source.card.cvc
        },
        billing_details: sourceInfo.source.owner
    });
}

/**
 * Attach Payment Methods to Customer
 * @param {object} sourceInfo 
 * @return {promise} attach
 */
async function attachPaymentMethod(sourceInfo) {
    console.log('Attach Payment Method to ', sourceInfo.customerId);
    return await stripe.paymentMethods.attach(sourceInfo.paymenet_method_id,{customer: sourceInfo.customerId});
}

/**
 * Deatch Payment Methods from Customer
 * @param {string} sourceId 
 * @return {promise} detach
 */
async function detachPaymentMethod(sourceId) {
    console.log('Detach Payment Method', sourceId);
    return await stripe.paymentMethods.detach(sourceId);
}

/**
 * List all Payment Methods of Customer
 * @param {string} customerId
 * @return {promise} list
 */
async function listAllPaymentMethod(customerId) {
    console.log('List Payment Method for ',customerId);
    return await stripe.paymentMethods.list({customer: customerId,type: 'card'});
}

/**
 * Clone Payment Methods to connected Account
 * @param {object} paymentInfo 
 * @return {promise} create
 */
async function cloningPaymentMethod(paymentInfo) {
    console.log('Cloning Payment Method');
    return await stripe.paymentMethods.create({
        customer: paymentInfo.customer,
        payment_method: paymentInfo.source
    }, {
        stripeAccount: paymentInfo.destination_account
    });
}

/**
 * Clone Payment Methods to bevvi Account
 * @param {object} paymentInfo 
 * @return {promise} create
 */
 async function cloningPaymentMethodForBevvi(paymentInfo) {
    console.log('Cloning Payment Method');
    return await stripe.paymentMethods.create({
        customer: paymentInfo.customer,
        payment_method: paymentInfo.source
    });
}

/**
 * Async Function for Source
 */
/**
 * Create Payment Source for Customer
 * @param {object} paymentInfo 
 * @return {promise} createSource
 */
async function attachSource(paymentInfo) {
    console.log(paymentInfo)
    console.log('Attach Source to Customer');
    return await stripe.customers.createSource(`${paymentInfo.customer}`, {source: paymentInfo.payment_method});
}

/**
 * Clone Payment Source to Connected Account
 * @param {object} paymentInfo 
 * @return {promise} create
 */
async function cloningSource(paymentInfo) {
    console.log('Clone Source to Connected Account');
    return await stripe.sources.create({
        customer: paymentInfo.customer,
        usage: 'reusable',
        original_source: paymentInfo.source
    }, {
        stripeAccount: paymentInfo.destination_account
    });
}

/**
 * Create Token for Cloning
 * @param {string} customerId,sourceId,connectedAccountId
 * @return {promise} create
 */
async function createTokenForCloning(customerId, sourceId,connectedAccountId) {
    console.log('Creating Token for Customer ID');
    return await stripe.tokens.create({customer:customerId, source:sourceId},{stripeAccount:connectedAccountId})
}


/**
 * Async Function for PaymentIntents
 */

/**
 * Create Payment Intents
 * @param {object} paymentInfo 
 * @return {promise} create
 */
async function createPaymentIntents(paymentInfo) {
    console.log(paymentInfo);
    return await stripe.paymentIntents.create({
        amount: paymentInfo.amount,
        currency: "usd",
        payment_method_types: ['card'],
        // customer: paymentInfo.customer,
        payment_method: paymentInfo.payment_method,
        capture_method: "manual",
        // confirmation_method: "manual",
        setup_future_usage: "off_session",
        // confirm: true,
        // application_fee_amount: paymentInfo.bartenderAmount ? (Math.round(paymentInfo.bartenderAmount*100)).toString() : 0,
        //application_fee_amount: '100'
    }, {
        stripeAccount: paymentInfo.destination_account
    })
}

/**
 * Create Payment Intents For Bevvi
 * @param {object} paymentInfo 
 * @return {promise} create
 */
 async function createPaymentIntentsForBevvi(paymentInfo) {
    console.log(paymentInfo);
    return await stripe.paymentIntents.create({
        amount: paymentInfo.amount,
        currency: "usd",
        payment_method_types: ['card'],
        customer: paymentInfo.customer,
        payment_method: paymentInfo.payment_method ? paymentInfo.payment_method : paymentInfo.source,
        capture_method: "manual",
        // confirmation_method: "manual",
        setup_future_usage: "off_session",
        // confirm: true,
        // application_fee_amount: paymentInfo.bartenderAmount ? (Math.round(paymentInfo.bartenderAmount*100)).toString() : 0,
        //application_fee_amount: '100'
    })
}

/**
 * Confirm Payment Intents
 * @param {string} paymentIntentsId paymentMethodId connectedAccountId
 * @return {promise} confirm
 */
async function confirmPaymentIntents(paymentIntentsId, paymentMethodId, connectedAccountID) {
    console.log('Confirm Payment Intens');
    return await stripe.paymentIntents.confirm(paymentIntentsId, {
        payment_method: paymentMethodId,
        return_url: 'http://localhost:4200/cart-placeorder'
    },{
        stripeAccount: connectedAccountID
    });
}

/**
 * Cancel Payment Intents
 * @param {string} chargeId
 * @return {promise} cancel
 */
async function cancelPaymentIntents(chargeId, connectedAccountID) {
    console.log('Cancel Payment Intents');
    console.log('Charge ID',chargeId);
    return await stripe.paymentIntents.cancel(chargeId, {
        stripeAccount: connectedAccountID
    });
}


/**
 * Capture Payment Intents
 * @param {string} chargeId
 * @return {promise} capture
 */
async function capturePaymentIntents(chargeId, amount, connectedAccountID) {
    console.log('Capture Payment ID');
    console.log('Charge ID',chargeId);
    return await stripe.paymentIntents.capture(chargeId,{
        amount_to_capture: Number(amount * 100).toFixed(0)
    }, {
        stripeAccount: connectedAccountID
    });
}


/**
 * Refund Payment Intents
 * @param {string} paymentIntentsId
 * @return {promise} create
 */
async function refundPaymentIntents(paymentIntentsId, amount, connectedAccountID) {
    console.log('Refund Payment Intents');
    console.log('Payment Intent Id', paymentIntentsId);
    console.log('Refund amount', amount);
    return await stripe.refunds.create({
        payment_intent: paymentIntentsId,
        amount: Number(amount * 100).toFixed(0),
        refund_application_fee: false
    },{
        stripeAccount: connectedAccountID
    });
}

/**
 * Cancel Payment Intents For Bevvi
 * @param {string} chargeId
 * @return {promise} cancel
 */
 async function cancelPaymentIntentsForBevvi(chargeId) {
    console.log('Cancel Payment Intents For Bevvi');
    console.log('Charge ID',chargeId);
    return await stripe.paymentIntents.cancel(chargeId);
}


/**
 * Capture Payment Intents For Bevvi
 * @param {string} chargeId
 * @return {promise} capture
 */
async function capturePaymentIntentsForBevvi(chargeId, amount) {
    console.log('Capture Payment ID');
    console.log('Charge ID',chargeId);
    return await stripe.paymentIntents.capture(chargeId,{
        amount_to_capture: Number(amount * 100).toFixed(0)
    });
}


/**
 * Refund Payment Intents For Bevvi
 * @param {string} paymentIntentsId
 * @return {promise} create
 */
async function refundPaymentIntentsForBevvi(paymentIntentsId, amount) {
    console.log('Refund Payment Intents For Bevvi');
    console.log('Payment Intent Id', paymentIntentsId);
    console.log('Refund amount', amount);
    return await stripe.refunds.create({
        payment_intent: paymentIntentsId,
        amount: Number(amount * 100).toFixed(0),
        refund_application_fee: false
    });
}

// /**
//  * Setup Payment Intents
//  */
// async function setupPaymentIntents(paymentInfo) {
//     console.log(' In setup Payment Intents');
//     return await stripe.setupIntents.create({
//         payment_method: paymentInfo.paymenet_method_id,
//         customer: paymentInfo.customerId,
//         payment_method_types: ['card'],
//         usage: 'off_session',
//         confirm: true,
//     });
// }

async function sendCapturePaymentIntent(type, data) {
    if (type === 'bevvi') {
        return await capturePaymentIntentsForBevvi(data.charge_id, data.orderAmount)
    } else {
        return await capturePaymentIntents(data.charge_id, data.orderAmount, data.merchantAccountId)
    }
}

async function sendCancelPaymentIntent(type, data) {
    if (type === 'bevvi') {
        return await cancelPaymentIntentsForBevvi(data.charge_id)
    } else {
        return await cancelPaymentIntents(data.charge_id, data.merchantAccountId)
    }
}

async function sendRefundPaymentIntent(type, data) {
    if (type === 'bevvi') {
        return await refundPaymentIntentsForBevvi(data.charge_id, data.orderAmount)
    } else {
        return await refundPaymentIntents(data.charge_id, data.orderAmount, data.merchantAccountId)
    }
}

/**
 * transactionOrder Function to create Transaction Information for orders
 * @param {*} res 
 * @param {object/null} charge 
 * @param {HTTPError/null} err 
 * @param {object} paymentInfo 
 * @param {callback} callback 
 */
function transactionOrder(res, charge, err, paymentInfo, isCorpOrder=false, callback) {
    let shoppingCartBody = paymentInfo.shoppingCart;
    let chargeId;
    let stripeChargeError = false;
    let stripeError;
    let stripeErrorMsg;
    if (charge) {
        successlog.info(`transaction_order :: CHARGE CREATED ON USER :: ${JSON.stringify(charge)}`);
        console.log("transaction_order :: CHARGE CREATED ON USER :: ", charge);
        chargeId = charge.id;
    }

    if (err) {
        successlog.info(`transaction_order :: CHARGE CREATED ON USER :: error :: ${JSON.stringify(err)}`);
        console.log("transaction_order :: CHARGE CREATED ON USER :: error :: ", JSON.stringify(err));
        // res.send(JSON.stringify(err));
        stripeChargeError = true;
        stripeError = JSON.stringify(err);
        stripeErrorMsg = err.message.toUpperCase();
    }

    //CREATE TRANSACTION
    console.log('CREATE TRANSACTION');
    var options = {
        method: 'POST',
        url: B2B_API_ENDPOINT + 'transactions?access_token=' + paymentInfo.accessToken,
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            "accountId": paymentInfo.accountId,
            "bizAccountId": "DUMMY_ACCOUNT_ID",
            "chargeId": charge ? chargeId : "",
            "status": charge ? "CARD_AUTHORIZED" : stripeError ? err.code.toUpperCase() : 'CARD_UNAUTHORIZED',
            "orderId": isCorpOrder ? paymentInfo.shoppingCart.id: "",
            "customerPaymentId": paymentInfo.customer,
            "paymentCardId": paymentInfo.source,
            "merchantAccountId": paymentInfo.destination_account,
            "additionalData": charge ? charge : stripeError ? err : '',
            "tax": paymentInfo.taxAmount,
            "totalAmt": paymentInfo.amount.toString().substring(0, paymentInfo.amount.toString().length - 2) + "." + paymentInfo.amount.toString().substring(paymentInfo.amount.toString().length - 2),
            "paidDate": charge ? new Date(charge.created * 1000).toISOString() : new Date().toISOString(),
            "decline_code": stripeError ? err.raw.decline_code : "",
            "corpOrderId": isCorpOrder ? paymentInfo.corpOrderId : '',
            "isCorpOrder": isCorpOrder
        }
    };

    request(options, function (error, response, body) {
        if (error) {
            successlog.info(`transaction_order :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
            successlog.info(`transaction_order :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
        } else {
            successlog.info(`transaction_order :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
            console.log(JSON.stringify(body));
            transactionId = JSON.parse(body).id;
            if (isCorpOrder) {
                var options = {
                    method: 'PATCH',
                    url: B2B_API_ENDPOINT + 'corporders/' + paymentInfo.corpOrderId + '?access_token=' + paymentInfo.accessToken,
                    headers: { 'content-type': 'application/x-www-form-urlencoded' },
                    form: {
                        "decision": paymentInfo.decision ? paymentInfo.decision : 'fail',
                        "noFraudId": paymentInfo.noFraudId ? paymentInfo.noFraudId :''
                    }
                };
                request(options, function (error, response, body) {})
                var options = {
                    method: 'POST',
                    url: B2B_API_ENDPOINT + '/corporders/payCorpOrder?access_token=' + paymentInfo.accessToken,
                    headers: {'content-type': 'application/x-www-form-urlencoded'},
                    form: {
                        "accountId": paymentInfo.accountId,
                        "corpOrderId": paymentInfo.corpOrderId,
                        "totalAmt": parseFloat(paymentInfo.amount) / 100
                    }
                }

                request(options, function(error, response, body) {
                    if (error) {
                        successlog.info(`transaction_order :: CREATE PAY CORP ORDER :: error :: ${JSON.stringify(error)}`);
                        successlog.info(`transaction_order :: CREATE PAY CORP ORDER :: error :: ${JSON.stringify(body)}`);
                    } else {
                        successlog.info(`transaction_order :: CREATE PAY CORP ORDER :: error :: ${JSON.stringify(body)}`);
                        callback(body,'', res);
                    }
                })
            } else {
                tmpQuantity = 0;
                shoppingCartBody.forEach(function (item) {
                    tmpQuantity += item.quantity
                });
    
                let discountInfo = {};
                if (typeof paymentInfo.discountApplied == "string") {
                    discountInfo = JSON.parse(paymentInfo.discountApplied);
                } else if (typeof paymentInfo.discountApplied == "object") {
                    discountInfo = paymentInfo.discountApplied
                }
                
    
                //CREATE ORDER
                console.log('CREATE ORDER');
                var options = {
                    method: 'POST',
                    url: B2B_API_ENDPOINT + 'orders',
                    headers: { 'content-type': 'application/x-www-form-urlencoded' },
                    form: {
                        "accountId": paymentInfo.accountId,
                        "establishmentId": paymentInfo.establishmentId,
                        "deliveryTime": paymentInfo.deliveryDateTime,
                        "deliveryInstructions": paymentInfo.deliveryInstructions,
                        "totalAmount": paymentInfo.amount.substr(0, paymentInfo.amount.length - 2) + "." + paymentInfo.amount.substr(-2),
                        "originalTotalAmount": paymentInfo.amount.substr(0, paymentInfo.amount.length - 2) + "." + paymentInfo.amount.substr(-2),
                        "tax": parseFloat(paymentInfo.taxAmount).toFixed(2),
                        "qty": parseInt(tmpQuantity),
                        "taxPercent": (paymentInfo.taxPercent) ? parseFloat(paymentInfo.taxPercent) : 12.99,
                        "discountApplied": discountInfo,
                        "originalPromoCodeApplied": paymentInfo.promoCode ? paymentInfo.promoCode : '',
                        "deliveryFee": paymentInfo.deliveryFee,
                        "isShipping": paymentInfo.isShipping,
                        "shippingFee": paymentInfo.shippingFee ? paymentInfo.shippingFee : '0.00',
                        "orderForOther": paymentInfo.orderForOther,
                        "orderPhoneNumber": paymentInfo.orderPhoneNumber,
                        "tipAmt": paymentInfo.tipAmount,
                        "geoLocation": paymentInfo.deliveryLocation.geoLocation,
                        "address": paymentInfo.deliveryLocation.address,
                        "aptNumber": (paymentInfo.suitNumber != 'undefined') ? paymentInfo.suitNumber : "",
                        "locationId": paymentInfo.deliveryLocation.locationId,
                        "ipAddress": paymentInfo.ipAddress,
                        "userAgent": paymentInfo.userAgent,
                        "browserAcceptLanguage": paymentInfo.browserAcceptLanguage,
                        "status": stripeChargeError ? 6 : 0,
                        "message": stripeErrorMsg ? stripeErrorMsg : 'Order created successfuly',
                        "maxmindRiskScore": paymentInfo.riskScore ? paymentInfo.riskScore : 0.1,
                        "decision": paymentInfo.decision ? paymentInfo.decision : 'fail',
                        "noFraudId": paymentInfo.noFraudId ? paymentInfo.noFraudId :''
                    }
                };
    
                request(options, function (error, response, body) {
                    if (error) {
                        successlog.info(`transaction_order :: ORDER CREATED :: error :: ${JSON.stringify(error)}`);
                        successlog.info(`transaction_order :: ORDER CREATED :: error :: ${JSON.stringify(body)}`);
                        return true;
                    } else {
                        orderId = JSON.parse(body).id;
    
                        successlog.info(`transaction_order :: ORDER CREATED :: ${JSON.stringify(body)}`);
    
                        //UPDATE TRANSACTION WITH ORDER ID
                        console.log('UPDATE TRANSACTION '+transactionId+ ' WITH ORDER ID '+orderId);
                        var options = {
                            method: 'PATCH',
                            url: B2B_API_ENDPOINT + 'transactions/' + transactionId + '?access_token=' + paymentInfo.accessToken,
                            headers: { 'content-type': 'application/x-www-form-urlencoded' },
                            form: {
                                "orderId": orderId
                            }
                        };
    
                        request(options, function (error, response, body) {
                            if (error) {
                                successlog.info(`transaction_order :: UPDATE TRANSACTION WITH ORDER ID :: error :: ${JSON.stringify(error)}`);
                                successlog.info(`transaction_order :: UPDATE TRANSACTION WITH ORDER ID :: error :: ${JSON.stringify(body)}`);
                                return true;
                            } else {
                                successlog.info(`transaction_order :: UPDATE TRANSACTION WITH ORDER ID :: ${JSON.stringify(body)}`);
                                shoppingCartBody.forEach((element, index) => {
                                    var options = {
                                        method: 'POST',
                                        url: B2B_API_ENDPOINT + 'orderdetails',
                                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                                        form: {
                                            "productId": element.productId,
                                            "orderId": orderId,
                                            "price": element.offer.salePrice,
                                            "actualPrice": element.offer.originalPrice,
                                            "quantity": element.quantity,
                                            "offerId": element.offerId
                                        }
                                    };
    
                                    request(options, function (error, response, body) {
                                        if (error) {
                                            successlog.info(`transaction_order :: CREATE ORDERDETAIL :: error :: ${JSON.stringify(error)}`);
                                            successlog.info(`transaction_order :: CREATE ORDERDETAIL :: error :: ${JSON.stringify(body)}`);
                                        } else {
                                            successlog.info(`transaction_order :: CREATE ORDERDETAIL :: ${JSON.stringify(body)}`);
                                            if (index == (shoppingCartBody.length - 1)) {
    
                                                if (stripeChargeError) {
                                                    var options = {
                                                        method: 'GET',
                                                        url: B2B_API_ENDPOINT + 'orders/createStripeOrderNotification?orderId=' + orderId,
                                                        headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                    };
    
                                                    request(options);
                                                    callback('', stripeError, res);
                                                    
                                                } else {
                                                    //GIVE RESPONSE TO MOBILE DEVICE
                                                    var options = {
                                                        method: 'GET',
                                                        url: B2B_API_ENDPOINT + 'orders?filter={"where":{"id":"' + orderId + '","status":0},"include":["transactions","establishment",{"orderdetails":["product"]}]}',
                                                        headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                    };
    
                                                    request(options, function (error, response, body) {
                                                        if (error) {
                                                            successlog.info(`transaction_order :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(error)}`);
                                                            successlog.info(`transaction_order :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(body)}`);
                                                            callback('', err, res);
                                                            // res.send(JSON.stringify(err));
                                                            // res.end();
                                                        } else {
                                                            var options = {
                                                                method: 'GET',
                                                                url: B2B_API_ENDPOINT + 'orders/createNewOrderNotification?orderId=' + orderId,
                                                                headers: { 'content-type': 'application/x-www-form-urlencoded' }
                                                            };
    
                                                            request(options);
    
                                                            successlog.info(`transaction_order :: RESPONSE TO MOBILE :: ${JSON.stringify(body)}`);
                                                            callback(body,'', res);
                                                            // res.send(JSON.parse(body));
                                                            // res.end();
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            }

        }
    });
}

function chargeCallBack(result, error, response) {
    if (error) {
        if (typeof error === 'string') {
            error = JSON.parse(error);
        }
        response.status(error.statusCode).send(JSON.stringify(error));
    } else if (result) {
        if (typeof result === 'string') {
            result = JSON.parse(result);
        }
        response.status(200).send(JSON.stringify(result));
    }
    response.end();
}

function mutlipleTransactionCallBack(res, charge1, error1, paymentInfo_1, charge2, error2, paymentInfo_2, isCorpOrder=false) {
    console.log('Multiple Transaction Call Back');
    let result1;
    let result2;
    
    async.series([
        // Send TransactionOrder for charge1
        function (callback1) {
            transactionOrder(res, charge1, error1, paymentInfo_1, isCorpOrder, (result, error, response) => {
                if (result) {
                    if (typeof result === 'string') {
                        result = JSON.parse(result);
                    }
                    callback1(null, result);
                } else if (error) {
                    console.log('ERROR ------------1')
                    if (typeof error === 'string') {
                        error = JSON.parse(error);
                    }
                    callback1(error, '');
                }
            })
        },
        // Send TransactionOrder for charge2
        function (callback2) {
            transactionOrder(res, charge2, error2, paymentInfo_2, isCorpOrder, function(result, error, response) {
                if (result) {
                    if (typeof result === 'string') {
                        result = JSON.parse(result);
                    }
                    callback2(null, result);
                } else if (error) {
                    console.log('ERROR ------------')
                    if (typeof error === 'string') {
                        error = JSON.parse(error);
                    }
                    callback2(error, '');
                }
            });
        }
    ], function(err, result) {
        console.log(err);
        if (result != '') {
            res.status(200).send({ "message": "Order placed successfully.", "status": "success", "results": result }).end();
        } else if (err) {
            res.status(err.statusCode).send(JSON.stringify(err));
        }
    })
}

/**
 * Update Order Status after capturing the charge or releasing charge or refunding charge.
 * @param {number} status 
 * @param {string} order_id 
 * @param {HTTP_REQUEST} req 
 * @param {HTTP_RESPONSE} res 
 */
function orderStatusUpdate(status, order_id, orderAmount, isCorpOrder, req, res) {
    /**
     * Status : 1 = Order is accepted and payment is captured.
     * Status : 3 = Order is rejected by store and payment to captured is release.
     * Status : 5 = Order is rejected by store and payment is refund.
     */
    var options = {
        method: 'PATCH',
        url: B2B_API_ENDPOINT + 'orders/' + order_id + '?access_token=' + req.body.accessToken,
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        form: {
            "status": status,
            "originalTotalAmount": orderAmount
        }
    };
    if (isCorpOrder) {
        options = {
            method: 'PATCH',
            url: B2B_API_ENDPOINT + 'corporders/' + order_id + '?access_token=' + req.body.accessToken,
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            form: {
                "corpOrderStatus": status,
            }
        };    
    }

    request(options, function (error, response, body) {
        if (error) {
            successlog.info(`order_status :: UPDATE ORDER TO PENDING PICKUP :: error :: ${JSON.stringify(error)}`);
            successlog.info(`order_status :: UPDATE ORDER TO PENDING PICKUP :: error :: ${JSON.stringify(body)}`);
            return true;
        } else {
            successlog.info(`order_status :: UPDATE ORDER TO PENDING PICKUP :: ${JSON.stringify(body)}`);

            //GIVE RESPONSE TO MOBILE DEVICE
            var options = {
                method: 'GET',
                url: B2B_API_ENDPOINT + 'orders?filter={"where":{"id":"' + order_id + '"}}',
                headers: { 'content-type': 'application/x-www-form-urlencoded' }
            };

            request(options, function (error, response, body) {
                if (error) {
                    successlog.info(`order_status :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(error)}`);
                    successlog.info(`order_status :: RESPONSE TO MOBILE :: error :: ${JSON.stringify(body)}`);
                    res.send(JSON.stringify(err));
                    res.end();
                } else {
                    successlog.info(`order_status :: RESPONSE TO MOBILE :: ${JSON.stringify(body)}`);
                    res.send(JSON.parse(body));
                    res.end();
                }
            });
        }
    });
}

function cancelingOrder(res, paymentData, riskScore) {
    cancelPaymentIntents(paymentData.paymentIntent.id, paymentData.destination_account)
    .then((charge) => {
        console.log('Payment is Cancelled');
        console.log(charge);
        if (charge.status === 'canceled') {
            console.log('Check Risk Score and if above maxmind then send mail and empty remarkety cart');
            if (riskScore != 'pass') {
            // if (riskScore >= maxmindThreshold) {
                console.log('Score above threshold')
                // else if riskScore is more than define threshold then cancel the paymenIntent and
                // and also send a mail notification to DC , raja and shradesh
                async.parallel({
                    task1 : (cb) => {
                        if (process.env.NODE_ENV === 'production') {
                        
                            console.log('Send Email');
                            let options = {
                                method: 'POST',
                                url: B2B_API_ENDPOINT + 'bevviemails/emailHighRiskScoreOrder',
                                headers: {
                                    'Content-Type': 'application/json'
                                },
                                json: true,
                                body: {
                                    paymentData: paymentData,
                                    riskScore: riskScore
                                }
                            }; 
                
                            request(options, function (error, response, body) {
                                if (error) {
                                    console.log('False');
                                    console.log(error);
                                    cb('error',null)
                                } else if (response.statusCode === 200) {
                                    console.log('true')
                                    console.log(body);
                                    cb(null, true);
                                } else {
                                    console.log('False');
                                    console.log(body);
                                    cb('error',null)
                                }
                            });
                        } else {
                            cb(null, true);
                        }
                    },
                    task2: (cb) => {
                        if (process.env.NODE_ENV === 'production') {
                            console.log("Remove Cart from remarkety");
                            // const removeCart = (cb) => {
                                let cart = {
                                    id: paymentData.accountId,
                                    abandoned_checkout_url: 'https://getbevvi.com/cart-placeorder',
                                    email: paymentData.email,
                                    line_items: [],
                                    updatedAt: new Date().toISOString()
                                }
        
                                let options = {
                                    rejectUnauthorized: false,
                                    url: "https://webhooks.remarkety.com/webhooks/store/845eWQ4q",
                                    method: 'POST',
                                    headers: {
                                        'x-token': 'fff2428d819242b4533c635ebe55dea1b06f6eba',
                                        'x-event-type': 'carts/update',
                                        'Content-Type': 'application/json',
                                        'Cache-Control': 'no-cache'
                                    },
                                    body: JSON.stringify(cart)
                                }
        
                                request(options, function (err, res, body) {
                                    if (err) {
                                        console.log('Error', err);
                                        cb(null, true);
                                    } else {
                                        if (res.statusCode === 200) {
                                            console.log('Updated to Remarkety for Removing the cart');
                                            console.log(body);
                                        }
                                      cb(null, true);
                                    }
                                });
                            // }
                            // async.retry(2, removeCart, cb);
                        } else {
                            cb(null, true);
                        }
                    }
                }, (error, result) => {
                    if (error) {
                        res.status(400).send({'status': 'error', 'message': 'Payment processing error.'});
                    } else {
                        console.log('Async complete')
                        console.log("result true");
                        let message = {
                            order: 'canceled',
                            info: 'Payment processing problem.'
                        }

                        res.status(200).send({'status': 'paymentfailure', 'message': message});
                    }
                })
                
            } else {
                res.status(400).send({'status': 'error', 'message': 'Payment processing error.'});
            }
        }
        

    })
    .catch((error) => {
        // res.status(error.statusCode).send(error);
        return false;
    })
    // if riskScore is -1 only cancel the paymentIntent
    // console.log('Returning to main call')
    
}

function cancelingMultipleOrder(res, paymentInfo_1, paymentInfo1_maxmindRiskScore, paymentInfo_2, paymentInfo2_maxmindRiskScore) {
    async.parallel({
        paymentCancel1: (cb) => {
            cancelPaymentIntents(paymentInfo_1.paymentIntent.id, paymentInfo_1.destination_account)
            .then((charge) => {
                if (charge.status === 'canceled') {
                    if (paymentInfo1_maxmindRiskScore === 'fail') {
                    // if (paymentInfo1_maxmindRiskScore >= maxmindThreshold) {
                        console.log('Score above threshold')
                        // else if riskScore is more than define threshold then cancel the paymenIntent and
                        // and also send a mail notification to DC , raja and shradesh
                        async.parallel({
                            task1 : (cb) => {
                                if (process.env.NODE_ENV === 'development') {
                                
                                    console.log('Send Email');
                                    let options = {
                                        method: 'POST',
                                        url: B2B_API_ENDPOINT + 'bevviemails/emailHighRiskScoreOrder',
                                        headers: {
                                            'Content-Type': 'application/json'
                                        },
                                        json: true,
                                        body: {
                                            paymentData: paymentInfo_1,
                                            riskScore: paymentInfo1_maxmindRiskScore
                                        }
                                    }; 
                        
                                    request(options, function (error, response, body) {
                                        if (error) {
                                            console.log('False');
                                            console.log(error);
                                            cb('error',null)
                                        } else if (response.statusCode === 200) {
                                            console.log('true')
                                            console.log(body);
                                            cb(null, true);
                                        } else {
                                            console.log('False');
                                            console.log(body);
                                            cb('error',null)
                                        }
                                    });
                                } else {
                                    cb(null, true);
                                }
                            },
                            task2: (cb) => {
                                if (process.env.NODE_ENV === 'production') {
                                    console.log("Remove Cart from remarkety");
                                    // const removeCart = (cb) => {
                                        let cart = {
                                            id: paymentInfo_1.accountId,
                                            abandoned_checkout_url: 'https://getbevvi.com/cart-placeorder',
                                            email: paymentInfo_1.email,
                                            line_items: [{}],
                                            updatedAt: new Date().toISOString()
                                        }
                
                                        let options = {
                                            rejectUnauthorized: false,
                                            url: "https://webhooks.remarkety.com/webhooks/store/845eWQ4q",
                                            method: 'POST',
                                            headers: {
                                                'x-token': 'fff2428d819242b4533c635ebe55dea1b06f6eba',
                                                'x-event-type': 'carts/update',
                                                'Content-Type': 'application/json',
                                                'Cache-Control': 'no-cache'
                                            },
                                            body: JSON.stringify(cart)
                                        }
                
                                        request(options, function (err, res, body) {
                                            if (err) {
                                                console.log('Error', err);
                                                cb('error', null);
                                            } else {
                                              console.log('Updated to Remarkety for Removing the cart');
                                              console.log(body);
                                              cb(null, true);
                                            }
                                        });
                                    // }
                                    // async.retry(2, removeCart, cb);
                                } else {
                                    cb(null, true);
                                }
                            }
                        }, (error, result) => {
                            if (error) {
                                cb(true,null);
                            } else {
                                cb(null, true);
                            }
                        })
                        
                    } else {
                        cb(true, null);
                    }

                } else {
                    cb(true, null);
                }
            })
        },
        paymentCancel2: (cb) => {
            cancelPaymentIntents(paymentInfo_2.paymentIntent.id, paymentInfo_2.destination_account)
            .then((charge) => {
                if (charge.status === 'canceled') {
                    if (paymentInfo2_maxmindRiskScore === 'fail') {
                    // if (paymentInfo2_maxmindRiskScore >= maxmindThreshold) {
                    console.log('Score above threshold')
                        // else if riskScore is more than define threshold then cancel the paymenIntent and
                        // and also send a mail notification to DC , raja and shradesh
                        async.parallel({
                            task1 : (cb) => {
                                if (process.env.NODE_ENV === 'development') {
                                
                                    console.log('Send Email');
                                    let options = {
                                        method: 'POST',
                                        url: B2B_API_ENDPOINT + 'bevviemails/emailHighRiskScoreOrder',
                                        headers: {
                                            'Content-Type': 'application/json'
                                        },
                                        json: true,
                                        body: {
                                            paymentData: paymentInfo_2,
                                            riskScore: paymentInfo2_maxmindRiskScore
                                        }
                                    }; 
                        
                                    request(options, function (error, response, body) {
                                        if (error) {
                                            console.log('False');
                                            console.log(error);
                                            cb('error',null)
                                        } else if (response.statusCode === 200) {
                                            console.log('true')
                                            console.log(body);
                                            cb(null, true);
                                        } else {
                                            console.log('False');
                                            console.log(body);
                                            cb('error',null)
                                        }
                                    });
                                } else {
                                    cb(null, true);
                                }
                            },
                            task2: (cb) => {
                                if (process.env.NODE_ENV === 'production') {
                                    console.log("Remove Cart from remarkety");
                                    // const removeCart = (cb) => {
                                        let cart = {
                                            id: paymentInfo_2.accountId,
                                            abandoned_checkout_url: 'https://getbevvi.com/cart-placeorder',
                                            email: paymentInfo_2.email,
                                            line_items: [{}],
                                            updatedAt: new Date().toISOString()
                                        }
                
                                        let options = {
                                            rejectUnauthorized: false,
                                            url: "https://webhooks.remarkety.com/webhooks/store/845eWQ4q",
                                            method: 'POST',
                                            headers: {
                                                'x-token': 'fff2428d819242b4533c635ebe55dea1b06f6eba',
                                                'x-event-type': 'carts/update',
                                                'Content-Type': 'application/json',
                                                'Cache-Control': 'no-cache'
                                            },
                                            body: JSON.stringify(cart)
                                        }
                
                                        request(options, function (err, res, body) {
                                            if (err) {
                                                console.log('Error', err);
                                                cb('error', null);
                                            } else {
                                              console.log('Updated to Remarkety for Removing the cart');
                                              console.log(body);
                                              cb(null, true);
                                            }
                                        });
                                    // }
                                    // async.retry(2, removeCart, cb);
                                } else {
                                    cb(null, true);
                                }
                            }
                        }, (error, result) => {
                            if (error) {
                                
                            } else {
                                cb(null, true);
                            }
                        })
                        
                    } else {
                        cb(true, null);
                    }
                } else {
                    cb(true, null);
                }
            })
        }
    }, (err, result) => {
        if (err) {
            res.status(400).send({'status': 'error', 'message': 'Payment processing error.'});
        } else if (result.paymentCancel1 && result.paymentCancel2) {
            console.log('Async complete for payment Cancel')
            console.log("result true");
            let message = {
                order: 'canceled',
                info: 'Payment processing problem.'
            }

            res.status(200).send({'status': 'paymentfailure', 'message': message});
        } else {
            res.status(400).send({'status': 'error', 'message': 'Payment processing error.'});
        }
    })
}

router.get('/check-remarkety', function(req, res, next) {
    let cart = {
        id: '5d5310ca07a2b454eb2180cc',
        abandoned_checkout_url: 'https://getbevvi.com/cart-placeorder',
        email: 'shradesh@lotusis.com',
        line_items: [],
        updatedAt: new Date().toISOString()
    }

    let options = {
        rejectUnauthorized: false,
        url: "https://webhooks.remarkety.com/webhooks/store/845eWQ4q",
        method: 'POST',
        headers: {
            'x-token': 'fff2428d819242b4533c635ebe55dea1b06f6eba',
            'x-event-type': 'carts/update',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
        },
        body: JSON.stringify(cart)
    }

    request(options, function (err, res, body) {
        if (err) {
            console.log('Error', err);
            next(null, err);
        } else {
            if (res.statusCode === 200) {
                console.log('Updated to Remarkety for Removing the cart');
                console.log(body);
            }
          next(null, body);
        }
    });
})

router.post('/create_corp_charge_b2b', function (req, res, next) {
    console.log('Create Charge B2b Request Body', req.body);
    successlog.info(`create_corp_charge_b2b :: REQUEST BODY :: ${JSON.stringify(req.body)}`);
    const corpOrderId = req.body.corpOrderId;
    const bevviAccountId = req.body.accountId;
    const customerStripeId = req.body.cardSourceId;
    const accessToken = req.body.accessToken;
    const paymentToken = req.body.paymentToken;
    const bartenderAmount = req.body.bartender;
    const isBartender = bartenderAmount > 0 ? true : false;
    const establishmentArray = req.body.establishmentInfo;
    const paymentConfirmed = req.body.paymentConfirmed ? req.body.paymentConfirmed : [];
    const bevviCharge = req.body.bevviCharge;
    const bevviChargeConfirmed = req.body.bevviChargeConfirmed ? req.body.bevviChargeConfirmed : null
    const client = req.body.client ? req.body.client : null

    let ipAddress;
    let userAgent;
    let browserAcceptLanguage;

    console.log(req.headers['x-forwarded-for']);
    if (req.headers['x-forwarded-for']) {
        ipAddress = req.headers['x-forwarded-for'];
    }
    // Comment these line when using on dev server and prod server
    if (process.env.NODE_ENV === 'development') {
        // ipAddress = '103.255.182.250';
    }
    

    Object.entries(req.headers).forEach((key,res) => {
        if (key[0] === "user-agent") {
            userAgent = key[1];
        } else if (key[0] === "accept-language") {
            browserAcceptLanguage = key[1];
        }
    })
    let data = {
        establishmentArray: establishmentArray,
        accessToken: accessToken,
        paymentToken: paymentToken,
        customerStripeId: customerStripeId,
        bevviAccountId: bevviAccountId,
        browserAcceptLanguage: browserAcceptLanguage,
        ipAddress: ipAddress,
        userAgent: userAgent,
        corpOrderId: corpOrderId,
        bevviCharge: bevviCharge,
        bevviChargeConfirmed: bevviChargeConfirmed
    } 
    runEstablishment(data, paymentConfirmed, client, res)
});

async function runEstablishment(data, paymentConfirmed, client, res) {
    const establishmentArray = data.establishmentArray;
    const accessToken = data.accessToken;
    const paymentToken = data.paymentToken;
    const customerStripeId = data.customerStripeId;
    const bevviAccountId = data.bevviAccountId;
    const browserAcceptLanguage = data.browserAcceptLanguage;
    const ipAddress = data.ipAddress;
    const userAgent = data.userAgent;
    const corpOrderId = data.corpOrderId;
    const bevviCharge = data.bevviCharge;
    const bevviChargeConfirmed = data.bevviChargeConfirmed;

    let establishmentChargeArray = [];
    let errorProcessing = false;
    let orderData = [];
    let bevviChargeData = null;
    if (paymentConfirmed.length > 0) {
        if (process.env.NODE_ENV === 'production') {
            const orderObject = {
                corpOrderId,
                ipAddress,
            }

            await sendOrderDetailsToNoFraud(orderObject, async (result, error, response) => {
                if (result) {
                    const processData = {
                        accessToken,
                        establishmentArray,
                        paymentToken,
                        customerStripeId,
                        bevviAccountId,
                        browserAcceptLanguage,
                        ipAddress,
                        userAgent,
                        corpOrderId,
                        bevviCharge,
                        paymentConfirmed,
                        riskScore: result.decision,
                        decision: result.decision,
                        noFraudId: result.id,
                        status: 0
                    }
                    processPaymentConfirmed(processData, client, res);
                } else {
                    const processData = {
                        accessToken,
                        establishmentArray,
                        paymentToken,
                        customerStripeId,
                        bevviAccountId,
                        browserAcceptLanguage,
                        ipAddress,
                        userAgent,
                        corpOrderId,
                        bevviCharge,
                        paymentConfirmed,
                        riskScore: 'error',
                        decision: 'error',
                        noFraudId: 123,
                        status: 0
                    }
                    processPaymentConfirmed(processData, client, res);

                }
            });
        } else {
            const processData = {
                accessToken,
                establishmentArray,
                paymentToken,
                customerStripeId,
                bevviAccountId,
                browserAcceptLanguage,
                ipAddress,
                userAgent,
                corpOrderId,
                bevviCharge,
                paymentConfirmed,
                riskScore: '0.1',
                decision: 'pass',
                noFraudId: '123',
                status: 0
            }
            processPaymentConfirmed(processData, client, res);
        }
    } else {
        for (const [index, establishment] of establishmentArray.entries()) {
            console.log('Payment Processing for ', establishment.isBevviTotal ? 'Bevvi' : establishment.establishmentId)
            let paymentInfo = {
                accessToken: accessToken,
                amount: (Math.round(establishment.holdAmt*100)).toString(),
                actualAmount: establishment.holdAmt,
                currency: 'usd',
                customer: paymentToken,
                source: customerStripeId,
                destination_account: establishment.isBevviTotal ? BEVVI_STRIPE_ACC : establishment.paymentId,
                accountId: bevviAccountId,
                corpOrderId: corpOrderId,
                browserAcceptLanguage: browserAcceptLanguage,
                ipAddress: ipAddress,
                userAgent: userAgent,
                // shoppingCart: JSON.parse(body),
                taxAmount: establishment.taxes,
                count: 0,
                isBevviCharge: establishment.isBevviTotal,
                forEstId: establishment.isBevviTotal ? establishment.forEstId : ""
            }
    
            if (paymentConfirmed.length > 0) {
                if (process.env.NODE_ENV === 'production') {
                    const orderObject = {
                        corpOrderId,
                        ipAddress,
                    }
                    await sendOrderDetailsToNoFraud(orderObject, async (result, error, response) => {
                        if (error) {
                            errorProcessing = true;
                        }
                        if (result) { 
                            paymentInfo.riskScore = result.decision;
                            paymentInfo.decision = result.decision;
                            paymentInfo.noFraudId = result.id;
                            for (const data of paymentConfirmed) {
                                if (establishment.establishmentId === data.establishmentId) {
                                    paymentInfo.status === 0;
                                    paymentInfo.paymentIntent = data.paymentIntent;
                                    data.paymentInfo = paymentInfo;
    
                                    if (process.env.NODE_ENV === 'production') {
                                        console.log(`Send Data to NoFraud ${paymentInfo}`)
    
                                    } else {
                                        paymentInfo.riskScore = 0.1;
                                        paymentInfo.decision = 'pass';
                                        paymentInfo.noFraudId = '123';
                                        // data.paymentInfo = paymentInfo;
                                    }
                                }
                            }
    
                            if (index === establishmentArray.length - 1) {
                                if (paymentConfirmed.length > 0) {
                                    let payCorpData = {
                                        accountId: bevviAccountId,
                                        corpOrderInfoId: corpOrderId,
                                        noFraud: {
                                          riskScore: paymentInfo.riskScore,
                                          decision: paymentInfo.decision,
                                          noFraudId: paymentInfo.noFraudId
                                        },
                                        estInfo: []
                                    }
                                    let releaseFlag = true;
                                    for (const [i, data] of paymentConfirmed.entries()) {
                                        console.log('Index', i);
                                        if (!data.releaseCharge) {
                                        } else {
                                            releaseFlag = false;
                                        }
                                    }
    
                                    if (releaseFlag) {
                                        await createAllTransactionOnBackend(paymentConfirmed, accessToken,
                                            async (result, error, response) => {
                                                if (result.length > 0) {
                                                    payCorpData.estInfo = JSON.stringify(result)
                                                    callPayCorpOrderInfo(payCorpData, accessToken, client, res)
                                                }
                                            })
                                    }
                                } 
                            }
                        } else {
                            errorProcessing = true;
                        }
                    });
                } else {
                    for (const data of paymentConfirmed) {
                        if (establishment.establishmentId === data.establishmentId) {
                            paymentInfo.status === 0;
                            paymentInfo.paymentIntent = data.paymentIntent;
                            data.paymentInfo = paymentInfo;
    
                            if (process.env.NODE_ENV === 'production') {
                                console.log(`Send Data to NoFraud ${paymentInfo}`)
    
                            } else {
                                paymentInfo.riskScore = 0.1;
                                paymentInfo.decision = 'pass';
                                paymentInfo.noFraudId = '123';
                                // data.paymentInfo = paymentInfo;
                            }
                        }
                    }
    
                    if (index === establishmentArray.length - 1) {
                        if (paymentConfirmed.length > 0) {
                            let payCorpData = {
                                accountId: bevviAccountId,
                                corpOrderInfoId: corpOrderId,
                                noFraud: {
                                    riskScore: paymentInfo.riskScore,
                                    decision: paymentInfo.decision,
                                    noFraudId: paymentInfo.noFraudId
                                },
                                estInfo: []
                            }
                            let releaseFlag = true;
                            for (const [i, data] of paymentConfirmed.entries()) {
                                console.log('Index', i);
                                if (!data.releaseCharge) {
                                } else {
                                    releaseFlag = false;
                                }
                            }
    
                            if (releaseFlag) {
                                await createAllTransactionOnBackend(paymentConfirmed, accessToken,
                                    async (result, error, response) => {
                                        if (result.length > 0) {
                                            payCorpData.estInfo = JSON.stringify(result)
                                            callPayCorpOrderInfo(payCorpData, accessToken, client, res)
                                        }
                                    })
                            }
                        } 
                    }
                }
                
            } else {
                try {
                    if (establishment.isBevviTotal) {
                        // await cloningPaymentMethodForBevvi(paymentInfo)
                        // .then(async (source) => {
                        //     console.log(`Cloning Source Result for Bevvi is ${JSON.stringify(source)}`)
                        //     paymentInfo.payment_method = source.id;
                            
                        // }).catch((err) => {
                        //     console.log(`Error Cloning Payment Method for ${establishment.establishmentId} is ${err}`);
                        //     errorProcessing = true;
                        // })
                        await createPaymentIntentsForBevvi(paymentInfo)
                        .then((charge) => {
                            console.log(`Charge Result for Bevvi is ${JSON.stringify(charge)}`);
                            if (charge.status === 'requires_confirmation') {
                                establishment.paymentId = BEVVI_STRIPE_ACC
                                establishmentChargeArray.push({
                                    establishmentData: establishment,
                                    chargeData: charge
                                });
                            } else if (charge.status === 'requires_capture') {
                                // Once Confirmed from frontend
                            } else {
                                errorProcessing = true;
                            }
                        }).catch((err) => {
                            console.log(`Error Creating Payment Intent for ${establishment.establishmentId} is ${JSON.stringify(err)}`);
                            errorProcessing = true;
                        })
                    } else {
                        await cloningPaymentMethod(paymentInfo)
                        .then(async (source) => {
                            console.log(`Cloning Source Result for ${establishment.establishmentId} is ${JSON.stringify(source)}`)
                            paymentInfo.payment_method = source.id;
                            await createPaymentIntents(paymentInfo)
                            .then((charge) => {
                                console.log(`Charge Result for ${establishment.establishmentId} is ${JSON.stringify(charge)}`);
                                if (charge.status === 'requires_confirmation') {
                                    establishmentChargeArray.push({
                                        establishmentData: establishment,
                                        chargeData: charge
                                    });
                                } else if (charge.status === 'requires_capture') {
                                    // Once Confirmed from frontend
                                } else {
                                    errorProcessing = true;
                                }
                            }).catch((err) => {
                                console.log(`Error Creating Payment Intent for ${establishment.establishmentId} is ${JSON.stringify(err)}`);
                                errorProcessing = true;
                            })
                        }).catch((err) => {
                            console.log(`Error Cloning Payment Method for ${establishment.establishmentId} is ${err}`);
                            errorProcessing = true;
                        })
                    }
                    if (index === establishmentArray.length -1 ) {
    
                        if (bevviCharge > 0) {
                            const bevviPaymentInfo = {
                                accessToken: accessToken,
                                amount: (Math.round(bevviCharge*100)).toString(),
                                actualAmount: bevviCharge,
                                currency: 'usd',
                                customer: paymentToken,
                                source: customerStripeId,
                                accountId: bevviAccountId,
                                corpOrderId: corpOrderId,
                                browserAcceptLanguage: browserAcceptLanguage,
                                ipAddress: ipAddress,
                                userAgent: userAgent,
                                payment_method: customerStripeId,
                                count: 0,
                            }
        
                            await createPaymentIntentsForBevvi(bevviPaymentInfo)
                                .then((charge) => {
                                    if (charge.status === 'requires_confirmation') {
                                        bevviChargeData = charge
                                    } else if (charge.status === 'requires_capture') {
    
                                    } else {
                                        errorProcessing = true;
                                    }
                                    let response = {
                                        code: 'success',
                                        corpOrderId: corpOrderId,
                                        accountId: bevviAccountId,
                                        cardSourceId: customerStripeId,
                                        accessToken: accessToken,
                                        paymentToken: paymentToken,
                                        chargeArray: establishmentChargeArray,
                                        bevviChargeData: bevviChargeData
                                    }
                                    if (errorProcessing) {
                                        response.code = 'error'
                                    }
                                
                                    res.status(200).send(response);
                                }).catch((err) => {
                                    console.log(`Error Creating Payment Intent for bevviChargeData is ${err}`);
                                    errorProcessing = true;
                                    if (errorProcessing) {
                                        response.code = 'error'
                                    }
                                
                                    res.status(200).send(response);
                                })
                            // let response = {
                            //     code: 'success',
                            //     corpOrderId: corpOrderId,
                            //     accountId: bevviAccountId,
                            //     cardSourceId: customerStripeId,
                            //     accessToken: accessToken,
                            //     paymentToken: paymentToken,
                            //     chargeArray: establishmentChargeArray,
                            //     bevviChargeData: bevviChargeData
                            // }
                            // if (errorProcessing) {
                            //     response.code = 'error'
                            // }
                        
                            // res.status(200).send(response);
                        } else {
                            let response = {
                                code: 'success',
                                corpOrderId: corpOrderId,
                                accountId: bevviAccountId,
                                cardSourceId: customerStripeId,
                                accessToken: accessToken,
                                paymentToken: paymentToken,
                                chargeArray: establishmentChargeArray,
                                bevviChargeData: bevviChargeData
                            }
                            if (errorProcessing) {
                                response.code = 'error'
                            }
                        
                            res.status(200).send(response);
                        }
                        
                    }
                } catch (err) {
                    console.log('Error');
                    res.status(200).send({"mesage: ":'Error fetching details'}).end();
                }
            }   
        }
    }
}

async function processPaymentConfirmed(processData, client, res) {
    const establishmentArray = processData.establishmentArray;
    const accessToken = processData.accessToken;
    const paymentToken = processData.paymentToken;
    const bevviAccountId = processData.bevviAccountId;
    const corpOrderId = processData.corpOrderId;
    const browserAcceptLanguage = processData.browserAcceptLanguage;
    const ipAddress = processData.ipAddress;
    const userAgent = processData.userAgent;
    const paymentConfirmed = processData.paymentConfirmed;
    const customerStripeId = processData.customerStripeId;
    const riskScore = processData.riskScore;
    const decision = processData.decision;
    const noFraudId = processData.noFraudId;
    const status = processData.status;
    
    for (const [index, establishment] of establishmentArray.entries()) {
        console.log('********************************')
        console.log('FOR EST', establishment.establishmentId, establishment.forEstId, establishment.holdAmt)
        console.log('********************************')
        let paymentInfo = {
            accessToken: accessToken,
            amount: (Math.round(establishment.holdAmt*100)).toString(),
            actualAmount: establishment.holdAmt,
            currency: 'usd',
            customer: paymentToken,
            source: customerStripeId,
            destination_account: establishment.isBevviTotal ? BEVVI_STRIPE_ACC : establishment.paymentId,
            establishmentId: establishment.establishmentId,
            forEstId: establishment.isBevviTotal ? establishment.forEstId : "",
            accountId: bevviAccountId,
            corpOrderId: corpOrderId,
            browserAcceptLanguage: browserAcceptLanguage,
            ipAddress: ipAddress,
            userAgent: userAgent,
            // shoppingCart: JSON.parse(body),
            taxAmount: establishment.taxes,
            count: 0,
            isBevviCharge: false,
            riskScore,
            decision,
            noFraudId,
            status,
        }

        for (const data of paymentConfirmed) {
            if ((establishment.establishmentId === data.establishmentId && !establishment.forEstId) ||
            (establishment.forEstId === data.forEstId && !establishment.establishmentId)) {
                paymentInfo.status === 0;
                paymentInfo.paymentIntent = data.paymentIntent;
                data.paymentInfo = paymentInfo;
                console.log(data.paymentInfo.amount)
                break;
            }
        }

        
        if (index === establishmentArray.length - 1) {
            // for (const data of paymentConfirmed) {
            //     console.log('-----------------------------')
            //     console.log(data.paymentInfo)
            //     console.log(data.paymentInfo.establishmentId)
            //     console.log(data.paymentInfo.forEstId)
            //     console.log(data.paymentInfo.actualAmount)
            //     console.log('-----------------------------')
            // }
            // return;
            if (paymentConfirmed.length > 0) {
                let payCorpData = {
                    accountId: bevviAccountId,
                    corpOrderInfoId: corpOrderId,
                    noFraud: {
                        riskScore: paymentInfo.riskScore,
                        decision: paymentInfo.decision,
                        noFraudId: paymentInfo.noFraudId
                    },
                    estInfo: []
                }
                let releaseFlag = true;
                for (const [i, data] of paymentConfirmed.entries()) {
                    console.log('Index', i);
                    if (!data.releaseCharge) {
                    } else {
                        releaseFlag = false;
                    }
                }

                if (releaseFlag) {
                    await createAllTransactionOnBackend(paymentConfirmed, accessToken,
                        async (result, error, response) => {
                            if (result.length > 0) {
                                payCorpData.estInfo = JSON.stringify(result)
                                console.log(JSON.stringify(payCorpData))
                                // return;
                                callPayCorpOrderInfo(payCorpData, accessToken, client, res)
                            }
                        })
                }
            } 
        }
    }
}

async function createAllTransactionOnBackend(transactionData, accessToken, callback) {
    // console.log('CREATE ALL TRANSACTION ON BACKEND', JSON.stringify(transactionData));
    if (transactionData.length > 0) {
        let transactionBody = []
        let transactionIDArray = []
        for (const data of transactionData) {

            console.log(JSON.stringify(data));
            if (data.paymentInfo.destination_account !== BEVVI_STRIPE_ACC) {
                transactionIDArray.push({
                    establishmentId: data.establishmentId ? data.establishmentId : "",
                    amount: data.amount,
                    merchantAccountId: data.paymentInfo.destination_account
                })
            }
            transactionBody.push({
                "accountId": data.paymentInfo.accountId,
                "bizAccountId": "DUMMY_ACCOUNT_ID",
                "chargeId": data.chargeData ? data.chargeData.id : "",
                "status": "CARD_AUTHORIZED",
                "orderId": "",
                "customerPaymentId": data.paymentInfo.customer,
                "paymentCardId": data.paymentInfo.source,
                "merchantAccountId": data.paymentInfo.destination_account,
                "additionalData": data.chargeData,
                "tax": data.paymentInfo.taxAmount,
                "totalAmt": data.paymentInfo.amount.toString().substring(0, data.paymentInfo.amount.toString().length - 2) + "." + data.paymentInfo.amount.toString().substring(data.paymentInfo.amount.toString().length - 2),
                "paidDate": data.charge ? new Date(data.charge.created * 1000).toISOString() : new Date().toISOString(),
                "decline_code": "",
                "corpOrderId": data.paymentInfo.corpOrderId,
                "isCorpOrder": true,
                "isBevviCharge": data.paymentInfo.isBevviCharge ? true: false,
                "forEstId": data.paymentInfo.forEstId ? data.paymentInfo.forEstId : ""
            })
        }
        console.log(JSON.stringify(transactionBody));
        // console.log(transactionBody);
        // return;
        var options = {
            method: 'POST',
            url: B2B_API_ENDPOINT + 'bevviutils/updateTransaction?access_token=' + accessToken,
            json: true,
            body: {
                transactionArray: transactionBody
            }
        }

        await request(options, function(error, response, body) {
            if (error) {
                successlog.info(`transaction_order :: CREATE MULTIPLE TRANSACTION :: error :: ${JSON.stringify(error)}`);
                successlog.info(`transaction_order :: CREATE MULTIPLE TRANSACTION :: error :: ${JSON.stringify(body)}`);
                callback('', 'error', '');  
            } else {
                successlog.info(`transaction_order :: CREATE MULTIPLE TRANSACTION :: ${JSON.stringify(body)}`);
                successlog.info(`transaction_order :: CREATE MULTIPLE TRANSACTION ::`);
                // transactionId = JSON.parse(body).id;
                if (body.length > 0) {
                    for (const transaction of transactionIDArray) {
                        for (const data of body) {
                            console.log(transaction.establishmentId)
                            console.log(data.forEstId)
                            console.log(data.merchantAccountId)
                            if (transaction.merchantAccountId === data.merchantAccountId) {
                                transaction.transactionId = data.id
                            } else if (transaction.establishmentId === data.forEstId) {
                                transaction.bevviTransactionId = data.id
                            }
                        }
                    }
                    callback(transactionIDArray, '','');
                }
            }
        })
    }
}

async function createTransactionOnBackend(charge, err, paymentInfo, index, callback) {
    console.log('call create transaction on backend for ', index)
    let stripeError = false;
    if (charge) {
        successlog.info('Create transaction on backend', charge)

    } else if (err) {
        stripeError = true;
        successlog.info('Create transaction on backend', err)
    }

  //  console.log('CREATE TRANSACTION');
    //console.log(paymentInfo);
    var options = {
        method: 'POST',
        //url: B2B_API_ENDPOINT + 'transactions?access_token=' + paymentInfo.accessToken,
        url: B2B_API_ENDPOINT + 'bevviutils/updateTransaction?access_token=' + paymentInfo.accessToken,
        //headers: { 'content-type': 'application/x-www-form-urlencoded' },
        headers: { 'content-type': 'application/json' },
        //form: {
        json: true,
        body: {
            "accountId": paymentInfo.accountId,
            "bizAccountId": "DUMMY_ACCOUNT_ID",
            "chargeId": charge ? charge.id : "",
            "status": charge ? "CARD_AUTHORIZED" : stripeError ? err.code.toUpperCase() : 'CARD_UNAUTHORIZED',
            "orderId": "",
            "customerPaymentId": paymentInfo.customer,
            "paymentCardId": paymentInfo.source,
            "merchantAccountId": paymentInfo.destination_account,
            "additionalData": charge ? charge : stripeError ? err : '',
            "tax": paymentInfo.taxAmount,
            "totalAmt": paymentInfo.amount.toString().substring(0, paymentInfo.amount.toString().length - 2) + "." + paymentInfo.amount.toString().substring(paymentInfo.amount.toString().length - 2),
            "paidDate": charge ? new Date(charge.created * 1000).toISOString() : new Date().toISOString(),
            "decline_code": stripeError ? err.raw.decline_code : "",
            "corpOrderId": paymentInfo.corpOrderId,
            "isCorpOrder": true,
            "isBevviCharge": paymentInfo.isBevviCharge ? true: false
        }
    };

    console.log('send request for transaction backend')
    await request(options, function(error, response, body) {
    console.log('hello');
        if (error) {
            successlog.info(`transaction_order :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`);
            successlog.info(`transaction_order :: CREATE TRANSACTION :: error :: ${JSON.stringify(body)}`);
            callback('', 'error', '');  
        } else {
            successlog.info(`transaction_order :: CREATE TRANSACTION :: success :: ${JSON.stringify(body)}`);
            console.log(body.id);
            transactionId = body.id;
            callback(transactionId, '','');
        }
    })
    console.log('callback is called')
}

function callPayCorpOrderInfo(corpData, accessToken, client, res) {
    console.log('Calling PayCorpOrderInfo client', client);
    let url = B2B_API_ENDPOINT + '/corporderinfos/payCorpOrderInfo?access_token=' + accessToken;
    if (client === 'sendoso') {
        url = B2B_API_ENDPOINT + 'sendosoorderinfos/payCorpOrderInfo?access_token=' + accessToken;
    } else if (client === 'nuveen') {
        url = B2B_API_ENDPOINT + 'nuveenorderinfos/payCorpOrderInfo?access_token=' + accessToken;
    } else if (client === 'jll') {
        url = B2B_API_ENDPOINT + 'jllorderinfos/payCorpOrderInfo?access_token=' + accessToken;
    } else if (client === 'kayak') {
        url = B2B_API_ENDPOINT + 'kayakorderinfos/payCorpOrderInfo?access_token=' + accessToken;
    } else if (client.includes('tiaa')) {
      url = B2B_API_ENDPOINT + 'tiaaorderinfos/payCorpOrderInfo?access_token=' + accessToken;
    } else if (client.includes('ey')) {
      url = B2B_API_ENDPOINT + 'eorderinfos/payCorpOrderInfo?access_token=' + accessToken;
    }
    console.log('PAY CORP ORDER URL', url)
    var options = {
        method: 'POST',
        url,
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        form: corpData
    }
    console.log(options, corpData);
    console.log('Calling PayCorpOrderInfo', corpData);
    request(options, function(error, response, body) {
        if (error) {
            successlog.info(`transaction_order :: CREATE PAY CORP ORDER :: error :: ${JSON.stringify(error)}`);
            successlog.info(`transaction_order :: CREATE PAY CORP ORDER :: error :: ${JSON.stringify(body)}`);
        } else {
            successlog.info(`transaction_order :: CREATE PAY CORP ORDER :: success :: ${JSON.stringify(body)}`);
            res.status(200).send(body)
        }
    })
}

async function sendOrderDetailsToNoFraud(orderObject, callback) {
    var options = {
        url: B2B_API_ENDPOINT + 'bevviutils/nofraud-corp',
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        json: true,
        body: orderObject
    };

    request(options, function(error, response, body) {
        if (error) {
          successlog.info(`transaction_order :: NOFRAUD :: error :: ${JSON.stringify(error)}`);
        } else if (response.statusCode === 200) {
            successlog.info(`transaction_order :: NOFRAUD :: success :: ${JSON.stringify(body)}`);
            if (body.decision) {
              if (body.decision === 'pass') {
                  callback(body, '');
                  // orderData.push(paymentInfo);
                  // transactionOrder(res, req.body.paymentIntent, '', paymentInfo, true, chargeCallBack);
              } else {
                  callback('', 'error')
             }
            } else {
              callback('', 'error');
            }
        } else {
            callback('', 'error')
        }
    })
}

router.post('/capture_charge_bevvi', function (req, res, next) {
    var orderId = req.body.order_id;
    var access_token = req.body.access_token;
    var action = req.body.action;
    var chargeId = req.body.chargeId ? req.body.chargeId : 0;
    var amount = req.body.amount ? req.body.amount : 0;

    var url = `transactions?filter={"where":{"chargeId":${chargeId}, "isBevviCharge": true}}&access_token=${access_token}`
    var options = {
        method: 'GET',
        url: B2B_API_ENDPOINT + url,
        headers: {'content-type':'application/x-www-form-urlencoded'}
    }
    request(options, async function(error, response, body) {
        if (error) {
            successlog.info(`capture_charge_bevvi :: TRANSACTION FOR CURRENT ORDER :: error :: ${JSON.stringify(error)}`);
            successlog.info(`capture_charge_bevvi :: TRANSACTION FOR CURRENT ORDER :: error :: ${JSON.stringify(body)}`);
            res.status(200).send({ "message": "Error Occured", "status": "error" }).end();
        } else {
            successlog.info(`capture_charge_b2b :: TRANSACTION FOR CURRENT ORDER :: ${JSON.stringify(body)}`);
            if (JSON.parse(body).error) {
                res.status(200).send({ "message": JSON.parse(body).error.message, "status": "error", "status_code": "SESSION_EXPIRED" }).end();
            } else {
                transaction_detail = JSON.parse(body)[0];
                if (action === 'release') {
                    await cancelPaymentIntentsForBevvi(chargeId)
                    .then(async (result) => {
                        if (result.status === 'canceled') {
                            var options = {
                                method: 'POST',
                                url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                headers: {'content-type': 'application/x-www-form-urlencoded'},
                                form: {
                                    "accountId": transaction_detail.accountId,
                                    "bizAccountId": "DUMMY_ACCOUNT_ID",
                                    "chargeId": chargeId,
                                    "status": "CANCEL_CAPTURE",
                                    "orderId": orderId,
                                    "customerPaymentId": transaction_detail.customerPaymentId,
                                    "merchantAccountId": "bevvi",
                                    "additionalData": result,
                                    "paidDate": new Date(result.created * 1000).toISOString()
                                }
                            }

                            request(options, function(error, response, body) {
                                if (error) {
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                    res.status(400).send(JSON.stringify(error)).end();
                                } else {
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                }
                            })
                        }
                    }).catch((err) => {
                        console.log('Error Cancelling Capture');
                        console.log('Error Capturing',err);
                        successlog.info(`cancel_charge :: CAPTURE CHARGE :: error :: ${JSON.stringify(err)}`);
                        if (err.code == "charge_expired_for_capture") {
                            res.send({ "message": "Card Authorization is older than 7Days and is thus Expired", "status": "error" }).end();
                        }
                        else {
                            res.send({ "message": "Server Error", "status": "error" }).end();
                        }
                    });
                } else if (action === 'capture') {
                    await capturePaymentIntentsForBevvi(chargeId, amount)
                    .then(async (result) => {
                        if (result.status === 'canceled') {
                            var options = {
                                method: 'POST',
                                url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                headers: {'content-type': 'application/x-www-form-urlencoded'},
                                form: {
                                    "accountId": transaction_detail.accountId,
                                    "bizAccountId": "DUMMY_ACCOUNT_ID",
                                    "chargeId": chargeId,
                                    "status": "CARD_CHARGED",
                                    "orderId": orderId,
                                    "customerPaymentId": transaction_detail.customerPaymentId,
                                    "merchantAccountId": "bevvi",
                                    "additionalData": result,
                                    "paidDate": new Date(result.created * 1000).toISOString()
                                }
                            }

                            request(options, function(error, response, body) {
                                if (error) {
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                    res.status(400).send(JSON.stringify(error)).end();
                                } else {
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                }
                            })
                        }
                    }).catch((err) => {
                        console.log('Error Cancelling Capture');
                        console.log('Error Capturing',err);
                        successlog.info(`cancel_charge :: CAPTURE CHARGE :: error :: ${JSON.stringify(err)}`);
                        if (err.code == "charge_expired_for_capture") {
                            res.send({ "message": "Card Authorization is older than 7Days and is thus Expired", "status": "error" }).end();
                        }
                        else {
                            res.send({ "message": "Server Error", "status": "error" }).end();
                        }
                    });
                } else if (action === 'refund') {
                    await refundPaymentIntentsForBevvi(chargeId, amount)
                    .then(async (result) => {
                        if (result.status === 'canceled') {
                            var options = {
                                method: 'POST',
                                url: B2B_API_ENDPOINT + 'transactions?access_token=' + access_token,
                                headers: {'content-type': 'application/x-www-form-urlencoded'},
                                form: {
                                    "accountId": transaction_detail.accountId,
                                    "bizAccountId": "DUMMY_ACCOUNT_ID",
                                    "chargeId": chargeId,
                                    "status": "CARD_REFUNDED",
                                    "orderId": orderId,
                                    "customerPaymentId": transaction_detail.customerPaymentId,
                                    "merchantAccountId": "bevvi",
                                    "additionalData": result,
                                    "paidDate": new Date(result.created * 1000).toISOString()
                                }
                            }

                            request(options, function(error, response, body) {
                                if (error) {
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: error :: ${JSON.stringify(error)}`)
                                    res.status(400).send(JSON.stringify(error)).end();
                                } else {
                                    successlog.info(`capture_charge_bevvi :: CREATE TRANSACTION :: ${JSON.stringify(body)}`);
                                }
                            })
                        }
                    }).catch((err) => {
                        console.log('Error Cancelling Capture');
                        console.log('Error Capturing',err);
                        successlog.info(`cancel_charge :: CAPTURE CHARGE :: error :: ${JSON.stringify(err)}`);
                        if (err.code == "charge_expired_for_capture") {
                            res.send({ "message": "Card Authorization is older than 7Days and is thus Expired", "status": "error" }).end();
                        }
                        else {
                            res.send({ "message": "Server Error", "status": "error" }).end();
                        }
                    });
                }
            }
        }
    })
    

})

module.exports = router;
