var winston = require('winston');
require('winston-daily-rotate-file');

// const consoleConfig = [
//     new winston.transports.Console({
//         'colorize': true
//     })
// ];

// const createLogger = new winston.Logger({
var createLogger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            colorize: true
        })
    ]
});


var successLogger = createLogger;
var transports = new (winston.transports.DailyRotateFile)({
    filename: './logs/access.log',
    name: 'access-file',
    level: 'info',
    json: false,
    datePattern: 'yyyy-MM-dd',
    prepend: true
});
// successLogger.add(winstonRotator, {
//     'name': 'access-file',
//     'level': 'info',
//     'filename': './logs/access.log',
//     'json': false,
//     'datePattern': 'yyyy-MM-dd-',
//     'prepend': true
// });
successLogger.add(transports);

module.exports = {
    'successlog': successLogger
};